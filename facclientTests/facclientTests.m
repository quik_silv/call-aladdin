//
//  facclientTests.m
//  facclientTests
//
//  Created by mark on 11/19/16.
//  Copyright © 2016 Square Potato. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RemoteJson.h"

@interface RemoteJsonTests : XCTestCase
{
    RemoteJson *r;
    NSDictionary *testIdentity;
    NSDictionary *client;
    NSMutableString *validRequestorUuid;
    NSData *imageFile;
    NSUserDefaults *defaults;
    NSArray *queryData;
}
@end

@implementation RemoteJsonTests

- (void)setUp {
    [super setUp];
    defaults = [NSUserDefaults standardUserDefaults];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    r = [[RemoteJson alloc] init];
    testIdentity = @{
                     @"identifier_for_vendor":@"1111",
                     @"name":@"testname",
                     @"email":@"testemail",
                     @"address":@"testaddress",
                     @"phone":@"+16505553434",
                     @"city":@"Miri",
                     @"country":@"Malaysia",
                     @"is_contractor": @"0",
                     @"company_name": @"",
                     @"years_of_experience": @"0",
                     @"company_address": @"",
                     @"work_categories":@"",
                     };
    
    validRequestorUuid = [[NSMutableString alloc] initWithString:@"26cbf46d-23ea-4ec4-acbe-2175746e3901"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setLocale:locale];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    //[dateFormatter setDateFormat:@"dd MMM yyyy"];
    client = @{
               @"requestor":validRequestorUuid,
               @"title":@"notitle",
               @"scope_of_work":@"noscope",
               @"address":@"noaddress",
               @"longitude":@"100",
               @"latitude":@"100",
               @"city":@"Miri",
               @"country":@"Malaysia",
               @"contractor":@"",
               @"preferred_start_datetime":[dateFormatter stringFromDate:[NSDate date] ],
               @"preferred_end_datetime":[dateFormatter stringFromDate:[NSDate date] ],
               @"work_category":@"plumber",
               };
    
    NSString *imagefilePath = [[NSBundle mainBundle] pathForResource:@"upload-from-images-placeholder" ofType:@"png"];
    NSURL *imagefileUrl = [NSURL fileURLWithPath:[NSString stringWithString:imagefilePath] ];
    imageFile = [NSData dataWithContentsOfURL:imagefileUrl];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testUpdateDataFromRemoteSourceWithUuid {
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"];
    NSString *uuid = @"26cbf46d-23ea-4ec4-acbe-2175746e3901";
    NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles/%@/", uuid];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Correct queryData format"];
    dispatch_async(dispatch_get_main_queue(), ^{
        queryData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
        [expectation fulfill];
    });
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        XCTAssert([queryData isKindOfClass:[NSArray class] ]);
        XCTAssertFalse([queryData isKindOfClass:[NSDictionary class] ]);
        if(error) {
            XCTFail(@"Expectation failed with error: %@", error);
        }
    }];
}

- (void)testUpdateDataFromRemoteSourceWithUrlQuery {
    //which returns all, but should return none.
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"];
    NSString *firebase_uuid = @"TNlycsQjx8QOpwKxNt7cNsZn2Gm1";
    NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles?firebase_uuid=%@", firebase_uuid];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Correct queryData format"];
    dispatch_async(dispatch_get_main_queue(), ^{
        queryData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
        [expectation fulfill];
    });
    [self waitForExpectationsWithTimeout:15.0 handler:^(NSError *error) {
        XCTAssert([queryData isKindOfClass:[NSArray class] ]);
        XCTAssertFalse([queryData isKindOfClass:[NSDictionary class] ]);
        if(error) {
            XCTFail(@"Expectation failed with error: %@", error);
        }
    }];
}

- (void)testRequestValidUserUuid {
    NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles/%@/", validRequestorUuid];
    NSArray *json = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
    XCTAssertEqual(21, [json count]);
}

- (void)testCreateNewUserProfileWithNoImageFile {
    [r addIdentity:testIdentity withImage:nil];
    NSArray *expectedJson = @[@{@"image":@"The submitted file is empty."}];
    XCTAssertEqualObjects([expectedJson valueForKey:@"image"], [[r responseJson] valueForKey:@"image"], @"");
}

- (void)testCreateNewUserProfile {
    [r addIdentity:testIdentity withImage:imageFile];
    NSLog(@"%@", [r responseJson]);
    XCTAssertEqual(201, [r responseStatusCode], @"");
}

- (void)testCreateDuplicateUserProfile {
    [r addIdentity:testIdentity withImage:imageFile];
    XCTAssertNotEqual(201, [r responseStatusCode], @"");
}

- (void)testUserDataNotInStorage {
    XCTAssertNil([defaults objectForKey:@"userProfileData"]);
}

- (void)testEditUserProfile {
    
}

- (void)testDeleteUserProfile {
    
}

- (void)testCreateNewRequestWithInvalidRequestorUuidNoImageFileNoVoiceFile {
    NSMutableDictionary *mClient = [NSMutableDictionary dictionaryWithDictionary:client];
    [mClient setValue:@"1111" forKey:@"requestor"];
    [r addRequest:(NSDictionary *)mClient withImage:nil withVoice:nil];
    NSString *expectedJson = @"{\"image\":[\"The submitted file is empty.\"],\"requestor\":[\"Invalid value.\"]}";
    XCTAssertEqualObjects(expectedJson, [r responseJson], @"");
}

- (void)testCreateNewRequestWithNoImageFileNoVoiceFile {
    [r addRequest:(NSDictionary *)client withImage:nil withVoice:nil];
    NSString *expectedJson = @"{\"image\":[\"The submitted file is empty.\"]}";
    XCTAssertEqualObjects(expectedJson, [r responseJson], @"");
}

- (void)testCreateNewRequestWithNoVoiceFile {
    [r addRequest:client withImage:imageFile withVoice:nil];
    XCTAssertEqual(201, [r responseStatusCode], @"");
}

- (void)testEditRequest {
    
}

- (void)testFlagDeleteRequest {
    
}

- (void)testRateRequest {
    
}

- (void)testFlagFoundContractorRequest {
    
}
//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
