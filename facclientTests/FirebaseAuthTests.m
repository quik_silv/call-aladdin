//
//  FirebaseAuthTests.m
//  facclientTests
//
//  Created by Mark Wong on 2018/08/03.
//  Copyright © 2018 Square Potato. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SettingsTableViewController.h"

@import Firebase;
@import FirebaseAuth;
@import FirebaseUI;
@import FirebaseInstanceID;
@interface FirebaseAuthTests : XCTestCase

@end

@implementation FirebaseAuthTests

- (void)setUp {
    [super setUp];
    [FIRApp configure];
    [FIRAuth auth].settings.appVerificationDisabledForTesting = YES;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFirebaseUserPhoneSignOut {
    SettingsTableViewController *controller = [[SettingsTableViewController init] alloc];
    XCTAssertTrue([controller signOut]);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
