//
//  facclientUITests.m
//  facclientUITests
//
//  Created by mark on 11/19/16.
//  Copyright © 2016 Square Potato. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RemoteJson.h"

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseAuth;
@import FirebaseMessaging;
@interface facclientUITests : XCTestCase
{
    RemoteJson *r;
    XCUIApplication *app;
}
@end

@implementation facclientUITests

- (void)setUp {
    [super setUp];
    if ([FIRApp defaultApp] == nil) {
        [FIRApp configure];
    }
    [FIRAuth auth].settings.appVerificationDisabledForTesting = YES;
    // Put setup code here. This method is called before the invocation of each test method in the class.
    r = [[RemoteJson alloc] init];
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    app = [[XCUIApplication alloc] init];
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testEmailLogin {
    app.launchArguments = @[@"isUITesting"];
    [app launch];
    [app.buttons[@"Welcome-Login"] tap];
    [app.textFields[@"EmailLogin-Email"] tap];
    [app.textFields[@"EmailLogin-Email"] typeText:@"test@squarepotato.com"];
    [app.textFields[@"EmailLogin-Password"] tap];
    [app.textFields[@"EmailLogin-Password"] typeText:@"wrongpassword"];
    [app.buttons[@"EmailLogin-SignIn"] tap];
    XCTAssertNotNil(app.alerts[@"Error"]);
    [app.textFields[@"EmailLogin-Password"] tap];
    [app.textFields[@"EmailLogin-Password"] typeText:@"12345678"];
    [app.tabBars.buttons[@"Profile"] tap];
    XCTAssertEqualObjects([app.textFields[@"ProfileView-Phone"] value], @"+16505553434");
}
- (void)testUserRegistration {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    app.launchArguments = @[@"isUITesting"];
    [app launch];
    
    XCUIElementQuery *tablesQuery = app.tables;
    [app.buttons[@"Welcome-Register"] tap];
    [app.buttons[@"TnC-Agree"] tap];
    [app.textFields[@"UserRegistration-NameTextField"] tap];
    [app.textFields[@"UserRegistration-NameTextField"] typeText:@"noname"];
    [app.textFields[@"UserRegistration-Phone"] tap];
    [app.textFields[@"UserRegistration-Phone"] typeText:@"+16505553434"];
    [app.textFields[@"UserRegistration-Email"] tap];
    [app.textFields[@"UserRegistration-Email"] typeText:@"test@squarepotato.com"];
    [app.textFields[@"UserRegistration-Address"] tap];
    [app.textFields[@"UserRegistration-Address"] typeText:@"Lutong"];
    [tablesQuery.staticTexts[@"select your city"] tap];
    [tablesQuery.staticTexts[@"Miri"] tap];
    [tablesQuery.staticTexts[@"select your country"] tap];
    [tablesQuery.staticTexts[@"Malaysia"] tap];
    
    [app.buttons[@"UserRegistration-Done"] tap];
    
    XCTAssert(app.textFields[@"UserVerification-Code"]);
    [app.textFields[@"UserVerification-Code"] tap];
    [app.textFields[@"UserVerification-Code"] typeText:@"234567"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exists == 1"];
    XCUIElement *alert = app.alerts[@"Information"];
    XCTestExpectation *expectation = [self expectationForPredicate:predicate  evaluatedWithObject:alert handler:nil];
    
    [app.toolbars.buttons[@"Done"] tap];
    XCTAssertFalse([app.alerts[@"Information"] exists]);

    [self waitForExpectationsWithTimeout:20.f handler:^(NSError *error) {
        if(error) {
            XCTFail(@"Expectation Failed with error: %@", error);
        }
        XCTAssert([app.alerts[@"Information"] exists]);
        [expectation fulfill];
    }];
    XCTAssertNotNil(app.alerts[@"Information"]);
    [app.alerts[@"Information"].buttons[@"OK"] tap];
}

- (void)testFirebaseLinkNewEmail {
    app.launchArguments = @[@"isUITesting"];
    [app launch];
    [app.tables.switches[@"Password"] tap];
    [app.textFields[@"LinkEmail-NewPassword"] tap];
    [app.textFields[@"LinkEmail-NewPassword"] typeText:@"12345678"];
    [app.textFields[@"LinkEmail-ConfirmPassword"] tap];
    [app.textFields[@"LinkEmail-ConfirmPassword"] typeText:@"12345678"];
    [app.buttons[@"LinkEmail-Ok"] tap];
    XCTAssertNotNil(app.alerts[@"Information"]);
}
- (void)testContractorRegistration {
    app.launchArguments = @[@"isUITesting"];
    [app launch];
    
    XCUIElementQuery *tablesQuery = app.tables;
    [app.buttons[@"Welcome-Register"] tap];
    [app.buttons[@"TnC-Agree"] tap];
    [app.textFields[@"UserRegistration-NameTextField"] tap];
    [app.textFields[@"UserRegistration-NameTextField"] typeText:@"noname"];
    [app.textFields[@"UserRegistration-Phone"] tap];
    [app.textFields[@"UserRegistration-Phone"] typeText:@"+16505553434"];
    [app.textFields[@"UserRegistration-Email"] tap];
    [app.textFields[@"UserRegistration-Email"] typeText:@"mark@squarepotato.com"];
    [app.textFields[@"UserRegistration-Address"] tap];
    [app.textFields[@"UserRegistration-Address"] typeText:@"Lutong"];
    [tablesQuery.staticTexts[@"select your city"] tap];
    [tablesQuery.staticTexts[@"Miri"] tap];
    [tablesQuery.staticTexts[@"select your country"] tap];
    [tablesQuery.staticTexts[@"Malaysia"] tap];
    [app.tables.switches[@"Also register as Contractor"] tap];
    [app.tables.staticTexts[@"select your category"] tap];
    [[app.collectionViews.cells[@"JobCategories-plumber"].otherElements containingType:XCUIElementTypeImage identifier:@"water tap-01"].element tap];
    [app.navigationBars[@"CATEGORIES"].buttons[@"Done"] tap];
    [app.textFields[@"UserRegistration-CompanyName"] tap];
    [app.textFields[@"UserRegistration-CompanyName"] typeText:@"nocompany"];
    [app.textFields[@"UserRegistration-CompanyAddress"] tap];
    [app.textFields[@"UserRegistration-CompanyAddress"] typeText:@"nocompanyaddress"];
    [app.buttons[@"UserRegistration-Done"] tap];
    
    XCTAssert(app.textFields[@"UserVerification-Code"]);
    [app.textFields[@"UserVerification-Code"] tap];
    [app.textFields[@"UserVerification-Code"] typeText:@"234567"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exists == 1"];
    XCUIElement *alert = app.alerts[@"Information"];
    XCTestExpectation *expectation = [self expectationForPredicate:predicate  evaluatedWithObject:alert handler:nil];
    
    [app.toolbars.buttons[@"Done"] tap];
    XCTAssertFalse([app.alerts[@"Information"] exists]);
    
    [self waitForExpectationsWithTimeout:20.f handler:^(NSError *error) {
        if(error) {
            XCTFail(@"Expectation Failed with error: %@", error);
        }
        XCTAssert([app.alerts[@"Information"] exists]);
        [expectation fulfill];
    }];
    XCTAssertNotNil(app.alerts[@"Information"]);
    [app.alerts[@"Information"].buttons[@"OK"] tap];
}

- (void)testProfileView {
    [app launch];
    [app.tabBars.buttons[@"Profile"] tap];
    XCTAssertEqualObjects([app.textFields[@"ProfileView-Phone"] value], @"+16505553434");
}

- (void)testSwitchToContractor {
    [app launch];
    [app.tabBars.buttons[@"Profile"] tap];
    [app.tables.switches[@"Also register as Contractor"] tap];
    [app.tables.staticTexts[@"select your category"] tap];
    [[app.collectionViews.cells[@"JobCategories-plumber"].otherElements containingType:XCUIElementTypeImage identifier:@"water tap-01"].element tap];
    [app.navigationBars[@"CATEGORIES"].buttons[@"Done"] tap];
    [app.textFields[@"ProfileView-CompanyName"] tap];
    [app.textFields[@"ProfileView-CompanyName"] typeText:@"nocompany"];
    [app.textFields[@"ProfileView-CompanyAddress"] tap];
    [app.textFields[@"ProfileView-CompanyAddress"] typeText:@"nocompanyaddress"];
    [app.buttons[@"ProfileView-Edit"] tap];
}

- (void)testCreateRequest {
    [app launch];
    [[app.collectionViews.cells.otherElements containingType:XCUIElementTypeStaticText identifier:@"PLUMBER"].element tap];
    [app.textFields[@"AddRequest-Title"] tap];
    [app.textFields[@"AddRequest-Title"] typeText:@"notitle"];
    [app.textFields[@"AddRequest-ScopeOfWork"] tap];
    [app.textFields[@"AddRequest-ScopeOfWork"] typeText:@"noscope"];
    [app.textFields[@"AddRequest-Address"] tap];
    [app.textFields[@"AddRequest-Address"] typeText:@"Lutong"];
    [app.buttons[@"AddRequest-Submit"] tap];
    XCTAssertNotNil(app.alerts[@"Information"]);
    [app.alerts[@"Information"].buttons[@"OK"] tap];
    [app.tabBars.buttons[@"Dashboard"] tap];
}

- (void)testNonEmptyDashboard {
    [app launch];
    [app.tabBars.buttons[@"Dashboard"] tap];
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.tabBars.buttons[@"Dashboard"] tap];
    [[app.cells elementBoundByIndex:0] tap];
    
    XCTAssertEqualObjects([app.textFields[@"AddRequest-Title"] value], @"notitle");
    XCTAssertEqualObjects([app.textFields[@"AddRequest-ScopeOfWork"] value], @"noscope");
}
@end
