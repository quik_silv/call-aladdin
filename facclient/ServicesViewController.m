//
//  ServicesViewController.m
//  facclient
//
//  Created by mark on 7/30/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "ServicesViewController.h"
#import "RequestTableViewController.h"
#import "Spinner.h"
#import "Services.h"

@import Firebase;
@import FirebaseAuth;
@import FirebaseMessaging;
@interface ServicesViewController ()
{
    NSArray *services;
    NSArray *servicesName;
    NSArray *queryData;
}
@end

@implementation ServicesViewController
@synthesize collectionView;

- (void)viewDidLoad {
    [super viewDidLoad];
    //reset badge
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSMutableArray *tabbarViewControllers = [NSMutableArray arrayWithArray: [self.tabBarController viewControllers]];
    if ([FIRAuth auth].currentUser) {
        [tabbarViewControllers removeObjectAtIndex:5]; //remove signin
    } else {
        [tabbarViewControllers removeObjectAtIndex:4]; //remove profile
        [self.tabBarController setSelectedIndex:4];
    }
    [self.tabBarController setViewControllers: tabbarViewControllers ];
    
    if(![[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"is_contractor"] intValue]) {
        NSMutableArray *tabbarViewControllers = [NSMutableArray arrayWithArray: [self.tabBarController viewControllers]];
        [tabbarViewControllers removeObjectAtIndex:2];
        [self.tabBarController setViewControllers: tabbarViewControllers ];
    }
    
    Services *s = [[Services alloc] init];
    services = [s services];
    servicesName = [s servicesName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    return [services count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Return a cell
    UICollectionViewCell *newCell = [collectionView
                                     dequeueReusableCellWithReuseIdentifier:@"servicesCell"
                                     forIndexPath:indexPath];
    //Customize your cell here, default UICollectionViewCells do not contain any inherent
    //text or image views (like UITableView), but some could be added,
    //or a custom UICollectionViewCell sub-class could be used
    UIImageView *servicesImageView = (UIImageView *)[newCell viewWithTag:100];
    [servicesImageView setImage: [UIImage imageNamed:[services objectAtIndex:indexPath.row] ] ];
    UILabel *servicesLabel = (UILabel *)[newCell viewWithTag:99];
    [servicesLabel setText: [NSLocalizedString([servicesName objectAtIndex:indexPath.row], nil) uppercaseString] ];
    return newCell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"workCategorySegue"]) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
        RequestTableViewController *destViewController = segue.destinationViewController;
        [destViewController setSelectedService:[servicesName objectAtIndex:indexPath.row]];
        [destViewController setSelectedServiceIcon: [services objectAtIndex:indexPath.row]];
    }
}
@end
