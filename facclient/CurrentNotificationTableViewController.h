//
//  CurrentNotificationTableViewController.h
//  facclient
//
//  Created by mark on 2017/09/28.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CurrentNotificationTableViewController : UITableViewController <AVAudioPlayerDelegate>
@property (strong, nonatomic) NSString *selectedRequestUuid;
@property (weak, nonatomic) IBOutlet UILabel *workCategoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *workCategoryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *workTextField;
@property (weak, nonatomic) IBOutlet UITextField *preferredStartDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *preferredEndDateTextField;
@property (weak, nonatomic) IBOutlet UILabel *selectedCityLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedCountryLabel;


- (IBAction)makeCall:(id)sender;
- (IBAction)startPlay:(id)sender;
- (IBAction)stopPlay:(id)sender;

@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UILabel *remainingTimeLabel;

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag;
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error;
- (void)updateDisplay;
- (void)updateSliderLabels;
- (IBAction)currentTimeSliderValueChanged:(id)sender;
- (IBAction)currentTimeSliderTouchUpInside:(id)sender;

- (void) downloadVoiceMemo;
@end
