//
//  DashboardTableViewCell.h
//  facclient
//
//  Created by mark on 8/17/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *preferredDateRangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *scopeOfWorksLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@end
