//
//  Spinner.m
//  facclient
//
//  Created by mark on 3/17/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "Spinner.h"

@implementation Spinner
//UIView *activeView;
UIActivityIndicatorView *spinner;
- (UIView *)initWithView: (UIViewController *)parentViewController {
    self = [super init];
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = parentViewController.view.center;
    spinner.transform = CGAffineTransformMakeScale(1, 1);
    [spinner setColor:[UIColor orangeColor]];
    [parentViewController.view addSubview:spinner];
    return self;
}

- (void)start {
    [spinner startAnimating];
}

- (void)stop {
    [spinner stopAnimating];
}
@end
