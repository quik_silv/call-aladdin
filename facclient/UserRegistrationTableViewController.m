//
//  UserRegistrationTableViewController.m
//  facclient
//
//  Created by mark on 5/7/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "UserRegistrationTableViewController.h"
#import "CitiesTableViewController.h"
#import "CountriesTableViewController.h"
#import "JobCategoriesCollectionViewController.h"
#import "Spinner.h"
#import "RemoteJson.h"

@import Firebase;
@import FirebaseAuth;
@import FirebaseUI;

@interface UserRegistrationTableViewController () <FUIAuthDelegate>
{
    NSDictionary *identity;
    RemoteJson *r;
}
@end

@implementation UserRegistrationTableViewController
@synthesize authVerificationCode;
@synthesize nameLabel;
@synthesize nameTextField;
@synthesize phoneLabel;
@synthesize phoneTextField;
@synthesize emailLabel;
@synthesize emailTextField;
@synthesize addressLabel;
@synthesize addressTextField;
@synthesize contractorSwitch;
@synthesize photoButton;
@synthesize cityName;
@synthesize countryName;
@synthesize selectedCityLabel;
@synthesize selectedCountryLabel;
@synthesize workCategoryLabel;
@synthesize selectedWorkCategory;
@synthesize selectedServices;
@synthesize companyRegisteredAddressLabel;
@synthesize companyRegisteredAddressTextField;
@synthesize companyNameLabel;
@synthesize companyNameTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    r = [[RemoteJson alloc] init];
    
    nameTextField.delegate = (id)self;
    phoneTextField.delegate = (id)self;
    emailTextField.delegate = (id)self;
    addressTextField.delegate = (id)self;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
}

-(void)doneWithNumberPad{
    [phoneTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0.0;
    switch(indexPath.row) {
        case 0:
            height = 250.;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 11:
            height = 44.;
            break;
        case 8:
        case 9:
        case 10:
            height = 0.; //64.
            if ([contractorSwitch isOn]) {
                height = 64.;
            }
            break;
    }
    return height;
}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"selectJobCategorySegueForUserRegistration"])
    {
        JobCategoriesCollectionViewController *destViewController = [segue destinationViewController];
        if ([selectedServices count] == 0 ) {
            [destViewController setSelectedServices:@[] ];
        } else {
            [destViewController setSelectedServices:selectedServices];
        }
    }
    if ([[segue identifier] isEqualToString:@"enterUserVerificationSegue"]) {

    }
}

- (void)registerUser {
        identity = @{
                     @"identifier_for_vendor":[[[UIDevice currentDevice] identifierForVendor] UUIDString],
                     @"name":[nameTextField text],
                     @"email":[emailTextField text],
                     @"address":[addressTextField text],
                     @"phone":[phoneTextField text],
                     @"city":[selectedCityLabel text],
                     @"country":[selectedCountryLabel text],
                     @"is_contractor":[contractorSwitch isOn] ? @"1" : @"0",
                     @"company_name": [companyNameTextField text],
                     @"company_address": [companyRegisteredAddressTextField text],
                     @"work_categories":[contractorSwitch isOn] ?[selectedServices componentsJoinedByString:@";"] : @"",
                     };
        [self verifyUser];

}

-(void)verifyUser {
    Spinner *spin = [[Spinner alloc] initWithView:self];
    [spin start];
    dispatch_async(dispatch_get_main_queue(), ^{
        [r addIdentity:identity withImage:UIImagePNGRepresentation([[photoButton imageView] image]) ];
        
        NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles?phone=%@", [phoneTextField text] ];
        NSArray *queryData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
        // TODO: Need to find a better fix for this.
        [NSThread sleepForTimeInterval:10.0f]; //wait for 10 seconds for data to be uploaded
        if([queryData count]) {
        [[NSUserDefaults standardUserDefaults] setObject:[queryData objectAtIndex:0] forKey:@"userProfileData"];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
                                                            message:NSLocalizedString(@"Thank you for joining us.", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                message:NSLocalizedString(@"Unable to download from server. Please contact us.", nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        [spin stop];
    });
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *tabController = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
    [self presentViewController:tabController animated:YES completion:Nil];
}

- (IBAction)takePhoto:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    [actionSheet setDelegate:self];
    [actionSheet setTitle:NSLocalizedString(@"Photos", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Library", nil)];
    [actionSheet showInView:self.tableView];
}
-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    if(buttonIndex == 0) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else if(buttonIndex == 1) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        //        UIViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
        //        [self.navigationController pushViewController:controller animated:YES];
        //        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //        UITabBarController *tabController = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
        //        [self presentViewController:tabController animated:YES completion:Nil];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [photoButton setImage:chosenImage forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)toggleContractorSwitch:(id)sender {
    [self.tableView reloadData];
}
- (void)setSelectedCity:(NSString *)name {
    cityName = name;
}
- (void)setSelectedCountry:(NSString *)name {
    countryName = name;
}
-(IBAction)unwindFromCitySelectionToUserRegistration:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[CitiesTableViewController class]]) {
        [selectedCityLabel setText:cityName];
    }
}
-(IBAction)unwindFromCountrySelectionToUserRegistration:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[CountriesTableViewController class]]) {
        [selectedCountryLabel setText:countryName];
    }
}
-(IBAction)unwindFromJobCategorySelectionToUserRegistration:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[JobCategoriesCollectionViewController class]]) {
        [selectedWorkCategory setText:NSLocalizedString([selectedServices objectAtIndex:0], nil) ];
    }
}
- (IBAction)doneButton:(id)sender {
    UIColor *defaultColor = [UIColor blackColor];
    CGFloat defaultTextFieldBorderWidth = 0.0f;
    
    nameLabel.textColor = defaultColor;
    addressLabel.textColor = defaultColor;
    
    nameTextField.layer.borderWidth = defaultTextFieldBorderWidth;
    addressTextField.layer.borderWidth = defaultTextFieldBorderWidth;
    
    BOOL empty = NO;
    if ([[selectedCityLabel text] isEqualToString:@"select your city"]) {
        selectedCityLabel.textColor = [UIColor redColor];
        empty = YES;
    }
    if ([[selectedCountryLabel text] isEqualToString:@"select your country"]) {
        selectedCountryLabel.textColor = [UIColor redColor];
        empty = YES;
    }
    if ([[nameTextField text] length] == 0) {
        nameLabel.textColor = [UIColor redColor];
        nameTextField.layer.borderColor = [[UIColor redColor] CGColor];
        nameTextField.layer.borderWidth = 1;
        empty = YES;
    }
    if ([[phoneTextField text] length] == 0) {
        phoneLabel.textColor = [UIColor redColor];
        phoneTextField.layer.borderColor = [[UIColor redColor] CGColor];
        phoneTextField.layer.borderWidth = 1;
        empty = YES;
    }
    if ([[emailTextField text] length] == 0) {
        emailLabel.textColor = [UIColor redColor];
        emailTextField.layer.borderColor = [[UIColor redColor] CGColor];
        emailTextField.layer.borderWidth = 1;
        empty = YES;
    }
    if ([[addressTextField text] length] == 0) {
        addressLabel.textColor = [UIColor redColor];
        addressTextField.layer.borderColor = [[UIColor redColor] CGColor];
        addressTextField.layer.borderWidth = 1;
        empty = YES;
    }
    if ([contractorSwitch isOn]) {
        if ([[companyNameTextField text] length] == 0) {
            companyNameLabel.textColor = [UIColor redColor];
            companyNameTextField.layer.borderColor = [[UIColor redColor] CGColor];
            companyNameTextField.layer.borderWidth = 1;
            empty = YES;
        }
        if ([[companyRegisteredAddressTextField text] length] == 0) {
            companyRegisteredAddressLabel.textColor = [UIColor redColor];
            companyRegisteredAddressTextField.layer.borderColor = [[UIColor redColor] CGColor];
            companyRegisteredAddressTextField.layer.borderWidth = 1;
            empty = YES;
        }
        NSString *workCategoriesFlat = [selectedServices componentsJoinedByString:@";"];
        if ([workCategoriesFlat isEqualToString:@""]) {
            workCategoryLabel.textColor = [UIColor redColor];
            empty = YES;
        }
    } else {
        [companyNameTextField setText:@""];
        [companyRegisteredAddressTextField setText:@""];
    }
    if(!empty) {
        [FUIAuth defaultAuthUI].delegate = (id) self; // delegate should be retained by you!
        FUIPhoneAuth *phoneAuthProvider = [[FUIPhoneAuth alloc] initWithAuthUI:[FUIAuth defaultAuthUI]];
        [FUIAuth defaultAuthUI].providers = @[phoneAuthProvider];
        
        FUIPhoneAuth *phoneProvider = [FUIAuth defaultAuthUI].providers.firstObject;
        [phoneProvider signInWithPresentingViewController:self phoneNumber:[phoneTextField text] ];
    }
}
#pragma mark - FUIAuthDelegate methods

- (void)authUI:(FUIAuth *)authUI didSignInWithAuthDataResult:(nullable FIRAuthDataResult *)authDataResult
         error:(nullable NSError *)error {
    if (error) {
        if (error.code == FUIAuthErrorCodeUserCancelledSignIn) {
            [self showAlert:NSLocalizedString(@"User cancelled sign-in", nil)];
        } else {
            NSError *detailedError = error.userInfo[NSUnderlyingErrorKey];
            if (!detailedError) {
                detailedError = error;
            }
            [self showAlert:detailedError.localizedDescription];
        }
    } else {
        if ([FIRAuth auth].currentUser) {
            NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles?phone=%@", [FIRAuth auth].currentUser.phoneNumber];
            NSArray *queryData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
            if([queryData count]) {
                [[NSUserDefaults standardUserDefaults] setObject:[queryData objectAtIndex:0] forKey:@"userProfileData"];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UITabBarController *tabController = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
                [self presentViewController:tabController animated:YES completion:Nil];
            }else {
                [self registerUser];
            }
        } else {
            NSLog(@"Error");
        }
//        _labelUserEmail.text = authDataResult.user.email;
//        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionsSignedInAs]
//                      withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)showAlert:(NSString *)message {
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil)
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* closeButton =
    [UIAlertAction actionWithTitle:NSLocalizedString(@"Close", nil)
                             style:UIAlertActionStyleDefault
                           handler:nil];
    [alert addAction:closeButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end

