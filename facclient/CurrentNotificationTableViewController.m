//
//  CurrentNotificationTableViewController.m
//  facclient
//
//  Created by mark on 2017/09/28.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CurrentNotificationTableViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "RemoteJson.h"
#import "Spinner.h"
#import "Services.h"
#import "MapViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CurrentNotificationTableViewController ()
{
    NSDictionary *queryData;
    NSString *placeholderText;
    
    AVAudioPlayer *audioPlayer;
    AVAudioRecorder *audioRecorder;
    NSTimer* timer;
}
@end

@implementation CurrentNotificationTableViewController
@synthesize selectedRequestUuid;
@synthesize workCategoryLabel;
@synthesize workCategoryImageView;
@synthesize photoImageView;
@synthesize addressTextField;
@synthesize titleTextField;
@synthesize workTextField;
@synthesize preferredStartDateTextField;
@synthesize preferredEndDateTextField;
@synthesize selectedCityLabel;
@synthesize selectedCountryLabel;
@synthesize currentTimeSlider;
@synthesize remainingTimeLabel;

-(void)reloadData {
    Spinner *spin = [[Spinner alloc] initWithView:self];
    [spin start];
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests/%@/", selectedRequestUuid];
        queryData = [[r updateDataFromRemoteSourceFrom:dataSourceUrl] objectAtIndex:0];
        
        Services *s = [[Services alloc] init];
        NSArray *services = [s services];
        NSArray *servicesName = [s servicesName];
        
        NSURL *imageURL = [NSURL URLWithString:[queryData valueForKey:@"image"] ];
        [photoImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"upload-from-images-placeholder.png"] ];
        
        NSString *workCategoryIconName = [[NSString stringWithFormat:@"%@", [queryData valueForKey:@"work_category"] ] lowercaseString];
        NSString *workCategoryIconFilename = [services objectAtIndex:[servicesName indexOfObject:workCategoryIconName] ];
        
        [workCategoryImageView setImage:[UIImage imageNamed:workCategoryIconFilename ] ];
        [addressTextField setText:[queryData valueForKey:@"address"]  ];
        [titleTextField setText:[queryData valueForKey:@"title"]  ];
        [workTextField setText:[queryData valueForKey:@"scope_of_work"]  ];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
        [dateFormatter setLocale:locale];
        [dateFormatter setTimeZone:timeZone];
        [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *startdate = [dateFormatter dateFromString:[queryData valueForKey:@"preferred_start_datetime"] ];
        
        NSDate *enddate = [dateFormatter dateFromString:[queryData valueForKey:@"preferred_end_datetime"] ];
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        
        [preferredStartDateTextField setText:[dateFormatter stringFromDate:startdate] ];
        [preferredEndDateTextField setText:[dateFormatter stringFromDate:enddate] ];
        [selectedCityLabel setText:[queryData valueForKey:@"city"]  ];
        [selectedCountryLabel setText:[queryData valueForKey:@"country"]  ];
        [workCategoryLabel setText:[NSString stringWithFormat:@"%@", NSLocalizedString([queryData valueForKey:@"work_category"], nil) ] ];
        [self downloadVoiceNote];
        [spin stop];
    });
}

- (void) downloadVoiceNote {
    //download the file in a seperate thread.
    NSString *recordedAudioPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/voicenote.caf"];
//    if([[NSFileManager defaultManager] fileExistsAtPath:recordedAudioPath]) {
//
//    } else {
        [remainingTimeLabel setText:@"Downloading..."];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *urlToDownload = [queryData valueForKey:@"voice_note"];
            NSURL  *url = [NSURL URLWithString:urlToDownload];
            NSData *urlData = [NSData dataWithContentsOfURL:url];
            if (urlData)
            {
                
                //saving is done on main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    [urlData writeToFile:recordedAudioPath atomically:YES];
                    
                    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
                    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
                    
                    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithString:recordedAudioPath] ];
                    NSError *error;
                    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
                    audioPlayer.numberOfLoops = 0;
                    [currentTimeSlider setMaximumValue:audioPlayer.duration];
                    [self updateSliderLabels];
                });
            }
        });
//    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"selectGeoCoordinatesForNotifications"])
    {
        // Get reference to the destination view controller
        // Pass any objects to the view controller here, like...
        MapViewController *vc = [segue destinationViewController];
        [vc setEnableAnnotation:NO];
        NSLog(@"%f %f", [[queryData valueForKey:@"latitude"] doubleValue], [[queryData valueForKey:@"longitude"] doubleValue]);
        [vc setLocationCoordinate:CLLocationCoordinate2DMake([[queryData valueForKey:@"latitude"] doubleValue], [[queryData valueForKey:@"longitude"] doubleValue]) ] ;
    }
}

- (IBAction)makeCall:(id)sender {
    if([[queryData valueForKey:@"status"] isEqualToString:@"pending"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"is_contractor"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil)
                                                            message:NSLocalizedString(@"You are going to call the client.", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView setTag:100];
        [alertView show];
    }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag==100 && buttonIndex == [alertView cancelButtonIndex]) {
        RemoteJson *r = [[RemoteJson alloc] init];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles/%@", [queryData valueForKey:@"requestor"] ];
            NSArray *clientProfileData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
            if (![clientProfileData count]) {
                
            } else {
                NSString *clientPhoneNumber = [NSString stringWithFormat:@"tel://%@", [clientProfileData valueForKey:@"phone"] ];
#if !TARGET_OS_SIMULATOR
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:clientPhoneNumber]];
#endif
                [r flagFoundContractorRequest:selectedRequestUuid withContractor:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"firebase_uuid"]];
            }
        });
    }
}

- (IBAction)startPlay:(id)sender {
    // Init audio with playback capability
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSString *recordedAudioPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/voicenote.caf"];
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithString:recordedAudioPath] ];
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];
    [currentTimeSlider setMaximumValue:audioPlayer.duration];
    timer = [NSTimer
             scheduledTimerWithTimeInterval:0.1
             target:self selector:@selector(timerFired:)
             userInfo:nil repeats:YES];
}
-(void)timerFired:(NSTimer*) timer {
    [self updateDisplay];
}
-(void)stopTimer {
    [timer invalidate];
    timer = nil;
}
-(IBAction)stopPlay:(id)sender {
    [audioPlayer stop];
    [self stopTimer];
    [self updateDisplay];
}
#pragma mark - Display Update
- (void)updateDisplay
{
    NSTimeInterval currentTime = audioPlayer.currentTime;
//    NSString* currentTimeString = [NSString stringWithFormat:@"%.02f", currentTime];
    
    currentTimeSlider.value = currentTime;
    [self updateSliderLabels];
    
    //    self.currentTimeLabel.text = currentTimeString;
    //    self.playingLabel.text = self.player.playing ? @"YES" : @"NO";
    //    self.deviceCurrentTimeLabel.text = [NSString stringWithFormat:@"%.02f", self.player.deviceCurrentTime];
}

- (void)updateSliderLabels
{
    NSTimeInterval currentTime = currentTimeSlider.value;
//    NSString* currentTimeString = [NSString stringWithFormat:@"%.02f", currentTime];
    
    //self.elapsedTimeLabel.text =  currentTimeString;
    [remainingTimeLabel setText:[NSString stringWithFormat:@"%.02f s", audioPlayer.duration - currentTime] ];
}
- (IBAction)currentTimeSliderValueChanged:(id)sender
{
    if(timer)
        [self stopTimer];
    [self updateSliderLabels];
}
- (IBAction)currentTimeSliderTouchUpInside:(id)sender
{
    [audioPlayer stop];
    audioPlayer.currentTime = currentTimeSlider.value;
    [audioPlayer prepareToPlay];
    [self startPlay:self];
}
#pragma mark - AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"%s successfully=%@", __PRETTY_FUNCTION__, flag ? @"YES" : @"NO");
    [self stopTimer];
    [self updateDisplay];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"%s error=%@", __PRETTY_FUNCTION__, error);
    [self stopTimer];
    [self updateDisplay];
}
@end
