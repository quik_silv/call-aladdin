//
//  main.m
//  facclient
//
//  Created by mark on 11/19/16.
//  Copyright © 2016 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
