#import <Foundation/Foundation.h>
#import "RemoteJson.h"
#import "Reachability.h"

@interface RemoteJson ()

@end

@implementation RemoteJson
@synthesize responseJson;
@synthesize responseStatusCode;
NSURLSession *session;
NSMutableURLRequest *request;

- (id)updateDataFromRemoteSourceFrom:(NSString *) urlString
{
    NSMutableString *requestUrl = [NSMutableString stringWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
    [requestUrl replaceOccurrencesOfString:@"+" withString:@"%2B" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [requestUrl length])];
    //Temporary hack to take care of ampersands in query strings.
    [requestUrl replaceOccurrencesOfString:@"%20&%20" withString:@"%20%26%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [requestUrl length])];
    [requestUrl replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [requestUrl length])];
    [requestUrl replaceOccurrencesOfString:@"https://" withString:@"https://ng4n25jVFKzLA6He:qw6LjdrTcWSK5nqf@" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [requestUrl length])];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:requestUrl] ];
    
    NSError *jsonError = nil;
    NSHTTPURLResponse *jsonResponse = nil;
    NSData *response;
    do {
        response = [NSURLConnection sendSynchronousRequest:request returningResponse:&jsonResponse error:&jsonError];
        if (jsonError.code == -1005)
        {
            //response = [NSURLConnection sendSynchronousRequest:request returningResponse:&jsonResponse error:&jsonError];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                message:@"Due to network problems we are unable to access your data. Please try again later."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } while ([jsonError domain] == NSURLErrorDomain);
    responseStatusCode = (long)[jsonResponse statusCode];
    NSError* error;
    if(response) {
        responseJson = [NSJSONSerialization
                JSONObjectWithData:response
                options:kNilOptions
                error:&error];

        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        /** new data to be added **/
    } else {
        NSLog(@"No response from %@", request);
    }
    
    NSMutableArray *prunedArray = [[NSMutableArray alloc] init];
    //to handle arrays returning with dictionaries that contain null values
    if ([responseJson isKindOfClass:[NSArray class]]) {
        if([responseJson count]) {
            for (int i=0; i<[responseJson count]; i++) {
                NSDictionary *original = [responseJson objectAtIndex:i];
                NSMutableDictionary *prunedDictionary = [NSMutableDictionary dictionary];
                for (NSString * key in [original allKeys])
                {
                    if (![[original objectForKey:key] isKindOfClass:[NSNull class]])
                        [prunedDictionary setObject:[original objectForKey:key] forKey:key];
                }
                [prunedArray addObject:prunedDictionary];
            }
        }
    } else if ([responseJson isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *prunedDictionary = [NSMutableDictionary dictionary];
        for (NSString * key in [responseJson allKeys])
        {
            if (![[responseJson objectForKey:key] isKindOfClass:[NSNull class]])
                [prunedDictionary setObject:[responseJson objectForKey:key] forKey:key];
        }
        [prunedArray addObject:prunedDictionary];
    }
    return [NSArray arrayWithArray:prunedArray];
}
/*
 HTTP PATCH for User Profiles
 */
- (bool) flagDeleteIdentity:(NSString *) uuid {
    NSString *boundary = [self randomStringWithLength:20];
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"status"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"deleted"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:(id)self delegateQueue:nil];
    
    //username and password value
    NSString *username = @"ng4n25jVFKzLA6He";
    NSString *password = @"qw6LjdrTcWSK5nqf"; //FTP: ng4n25jVFKzLA6He@houseofacacia.com
    
    //HTTP Basic Authentication
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authenticationValue = [authenticationData base64Encoding];
    
    //Set up your request
    NSString *urlString = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles/%@/", uuid  ];
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] ];
    
    // Set your user login credentials
    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"PATCH"];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length] ];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"%@", strData);
        // Process the response
//        NSLog(@"%@", response);
    }];
    [uploadTask resume];
    return true;
}
- (bool) editIdentity:(NSDictionary *) identity withImage:(NSData *) imageData {
    NSString *boundary = [self randomStringWithLength:20];
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *key in [identity allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [identity objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"uploadedImage.png\"\r\n", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData ];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    //username and password value
    NSString *username = @"ng4n25jVFKzLA6He";
    NSString *password = @"qw6LjdrTcWSK5nqf";
    
    //HTTP Basic Authentication
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authenticationValue = [authenticationData base64Encoding];
    
    //Set up your request
    NSString *urlString = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles/%@/", identity[@"uuid"]  ];
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] ];
    
    // Set your user login credentials
    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"PATCH"];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length] ];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@", strData);
        // Process the response
        NSLog(@"%@", response);
        
        NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles?identifier_for_vendor=%@", [[[UIDevice currentDevice] identifierForVendor] UUIDString] ];
        NSArray *queryData = [self updateDataFromRemoteSourceFrom:dataSourceUrl];
        if([queryData count]){
            [[NSUserDefaults standardUserDefaults] setObject:[queryData objectAtIndex:0] forKey:@"userProfileData"];
        }
    }];
    [uploadTask resume];
    return true;
}

/*
 HTTP POST generator for create User Profiles
 */
- (bool) addIdentity:(NSDictionary *) identity withImage:(NSData *) imageData {
    NSString *boundary = [self randomStringWithLength:20];
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *key in [identity allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [identity objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"uploadedImage.png\"\r\n", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData ];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:(id)self delegateQueue:nil];
    
    //username and password value
    NSString *username = @"ng4n25jVFKzLA6He";
    NSString *password = @"qw6LjdrTcWSK5nqf";
    
    //HTTP Basic Authentication
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authenticationValue = [authenticationData base64Encoding];
    
    //Set up your request
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://facontractor.herokuapp.com/api/user_profiles/"] ];
    
    // Set your user login credentials
    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length] ];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [queue addOperationWithBlock:^{
        NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if(data) {
                NSError *jerror;
                responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jerror];
            } else {
                NSLog(@"error = %@", error);
            }
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            responseStatusCode = (long)[httpResponse statusCode];
            dispatch_semaphore_signal(semaphore);
        }];
        
        [uploadTask resume];
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return true;
}

/*
 HTTP PATCH for REQUESTS
 */
- (bool) rateRequest:(NSDictionary *) queryData {
    NSString *boundary = [self randomStringWithLength:20];
    NSMutableData *body = [NSMutableData data];
    for (NSString *key in [queryData allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [queryData objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    //username and password value
    NSString *username = @"ng4n25jVFKzLA6He";
    NSString *password = @"qw6LjdrTcWSK5nqf";
    
    //HTTP Basic Authentication
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authenticationValue = [authenticationData base64Encoding];
    
    //Set up your request
    NSString *urlString = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests/%@/", [queryData objectForKey:@"uuid"]  ];
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] ];
    
    // Set your user login credentials
    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"PATCH"];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length] ];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@", strData);
        // Process the response
        NSLog(@"%@", response);
    }];
    [uploadTask resume];
    return true;
}
/*
 HTTP PATCH for REQUESTS
 */
- (bool) flagFoundContractorRequest:(NSString *) requestUuid withContractor:(NSString *) contractorFirebaseUuid {
    NSString *boundary = [self randomStringWithLength:20];
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"status"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"found_contractor"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"contractor.firebase_uuid"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", contractorFirebaseUuid] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    //username and password value
    NSString *username = @"ng4n25jVFKzLA6He";
    NSString *password = @"qw6LjdrTcWSK5nqf"; //FTP: ng4n25jVFKzLA6He@houseofacacia.com
    
    //HTTP Basic Authentication
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authenticationValue = [authenticationData base64Encoding];
    
    //Set up your request
    NSString *urlString = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests/%@/", requestUuid  ];
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] ];

    // Set your user login credentials
    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"PATCH"];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length] ];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@", strData);
        // Process the response
        NSLog(@"%@", response);
    }];
    [uploadTask resume];
    return true;
}
/*
 HTTP PATCH for REQUESTS
 */
- (bool) flagDeleteRequest:(NSString *) requestUuid {
    NSString *boundary = [self randomStringWithLength:20];
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"status"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"deleted"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    //username and password value
    NSString *username = @"ng4n25jVFKzLA6He";
    NSString *password = @"qw6LjdrTcWSK5nqf"; //FTP: ng4n25jVFKzLA6He@houseofacacia.com
    
    //HTTP Basic Authentication
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authenticationValue = [authenticationData base64Encoding];
    
    //Set up your request
    NSString *urlString = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests/%@/", requestUuid  ];
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] ];
    
    // Set your user login credentials
    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"PATCH"];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length] ];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@", strData);
        // Process the response
        NSLog(@"%@", response);
    }];
    [uploadTask resume];
    return true;
}

/*
 POST request generator for sending Requests
 */
- (bool) addRequest:(NSDictionary *) client withImage:(NSData *) imageData withVoice:(NSData *)voiceData {
    NSString *boundary = [self randomStringWithLength:20];
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *key in [client allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [client objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if([voiceData length]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"uploadedVoiceNote.caf\"\r\n", @"voice_note"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: audio/caf\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:voiceData ];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"uploadedImage.png\"\r\n", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData ];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    //username and password value
    NSString *username = @"ng4n25jVFKzLA6He";
    NSString *password = @"qw6LjdrTcWSK5nqf"; //FTP: ng4n25jVFKzLA6He@houseofacacia.com
    
    //HTTP Basic Authentication
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authenticationValue = [authenticationData base64Encoding];
    
    //Set up your request
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://facontractor.herokuapp.com/api/requests/"] ];
    
    // Set your user login credentials
    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length] ];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [queue addOperationWithBlock:^{
        NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            if(data) {
                responseJson = [[NSMutableString alloc] initWithString:strData];
            }
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            responseStatusCode = (long)[httpResponse statusCode];
            dispatch_semaphore_signal(semaphore);
        }];
        [uploadTask resume];
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return true;
}

/*
 HTTP PATCH for Requests
 */
- (bool) editRequest:(NSDictionary *) client withImage:(NSData *) imageData  withVoice:(NSData *) voiceData {
    NSString *boundary = [self randomStringWithLength:20];
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *key in [client allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [client objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if(voiceData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"uploadedVoiceMemo.caf\"\r\n", @"voice_note"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: audio/caf\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData ];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"uploadedImage.png\"\r\n", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData ];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    //username and password value
    NSString *username = @"ng4n25jVFKzLA6He";
    NSString *password = @"qw6LjdrTcWSK5nqf"; //FTP: ng4n25jVFKzLA6He@houseofacacia.com
    
    //HTTP Basic Authentication
    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authenticationValue = [authenticationData base64Encoding];
    
    //Set up your request
    NSString *urlString = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests/%@/", client[@"uuid"]  ];
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] ];
    
    // Set your user login credentials
    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"PATCH"];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length] ];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@", strData);
        // Process the response
        NSLog(@"%@", response);
    }];
    [uploadTask resume];
    return true;
}


- (NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform(62) % [letters length]]];
    }
    
    return randomString;
}
@end
