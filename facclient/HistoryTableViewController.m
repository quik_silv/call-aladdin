//
//  HistoryTableViewController.m
//  facclient
//
//  Created by mark on 8/5/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "HistoryTableViewController.h"
#import "CurrentHistoryTableViewController.h"
#import "RemoteJson.h"
#import "Spinner.h"
#import "HistoryTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface HistoryTableViewController ()
{
    NSArray *queryData;
}
@end

@implementation HistoryTableViewController
@synthesize selectedRequestUuid;
- (void) reloadData {
    Spinner *spin = [[Spinner alloc] initWithView:self];
    [spin start];
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        //requestor_uuid should be named requestor_indentifier_for_vendor. It is not the entry's uuid
        NSString *uuid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"uuid"];
        NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests?status=found_contractor&or_completed=1&requestor=%@", uuid]; //returns both found_contractor and completed jobs
        queryData = [[NSMutableArray alloc] initWithArray:[r updateDataFromRemoteSourceFrom:dataSourceUrl] ];
        if (![queryData count]) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
//                                                                message:NSLocalizedString(@"You have no completed requests", nil)
//                                                               delegate:self
//                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
//                                                      otherButtonTitles:nil];
//            [alertView show];
        } else {
//            NSMutableArray *modifiedArray = [queryData mutableCopy];
//            for (int q=0; q<[queryData count]; q++) {
//                NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles/%@", [[[queryData objectAtIndex:q] objectForKey:@"contractor"] objectForKey:@"uuid"] ];
//                NSArray *contractorData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
//
//                NSMutableDictionary *modifiedDictionary = [[queryData objectAtIndex:q] mutableCopy];
//                [modifiedDictionary setValue:[contractorData valueForKey:@"company_name"]  forKey:@"contractor_company_name"];
//                [modifiedDictionary setValue:[contractorData valueForKey:@"image"] forKey:@"contractor_image"];
//
//                [modifiedArray replaceObjectAtIndex:q withObject:modifiedDictionary];
//            }
//            queryData = [modifiedArray copy];
            [self.tableView reloadData];
        }
        [spin stop];
    });
}
- (void) getLatestData:(UIRefreshControl *)refreshControl {
    [self reloadData];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *updated = [NSString stringWithFormat:NSLocalizedString(@"Last update: %@", nil), [formatter stringFromDate:[NSDate date]]];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:updated];
    [self.refreshControl endRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    [self reloadData];
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor orangeColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestData:)
                  forControlEvents:UIControlEventValueChanged];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadData];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger numberOfSections = 0;
    if ([queryData count])
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numberOfSections = 1;
        self.tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
        noDataLabel.text             = NSLocalizedString(@"There are no pending job requests", nil);
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        self.tableView.backgroundView = noDataLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [queryData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 115;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"historyCell"];
    if(cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"HistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"historyCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"historyCell" forIndexPath:indexPath];
    }
    [cell.workCategoryLabel setText:[NSLocalizedString([[queryData objectAtIndex:indexPath.row] objectForKey:@"work_category"], nil) uppercaseString] ];
    
    NSString *statusStr = [[[queryData objectAtIndex:indexPath.row] objectForKey:@"status"] stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    [cell.statusLabel setText:NSLocalizedString(statusStr, nil) ];
    
    [cell.contractorLabel setText:[[[queryData objectAtIndex:indexPath.row] objectForKey:@"contractor"] objectForKey:@"company_name"] ];
    if ([[[queryData objectAtIndex:indexPath.row] objectForKey:@"rating"] isEqual:[NSNull null] ]) {
        [cell.rateLabel setText:NSLocalizedString(@"Rate your experience!", nil)];
    } else {
        [cell.rateLabel setText:[NSString stringWithFormat:NSLocalizedString(@"You rated %@", nil),[[queryData objectAtIndex:indexPath.row] objectForKey:@"rating"] ] ];
        [cell.createdLabel setHidden:NO];
        [cell.createdLabel setText:[[[[queryData objectAtIndex:indexPath.row] objectForKey:@"created"] substringToIndex:19] stringByReplacingOccurrencesOfString:@"T" withString:@" " ] ];
    }
    
    NSURL *imageURL = [NSURL URLWithString:[[[queryData objectAtIndex:indexPath.row] objectForKey:@"contractor"] objectForKey:@"image"] ];
    [cell.contractorImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"upload-from-images-placeholder.png"] options:SDWebImageRefreshCached];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedRequestUuid = [NSString stringWithFormat:@"%@", [[queryData objectAtIndex:indexPath.row] objectForKey:@"uuid"] ];
    [self performSegueWithIdentifier:@"current_history" sender:self];
}
//-(void)tableView:(UITableView*)tableView willDisplayCell:(HistoryTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    [cell.createdLabel setText:[[historyData objectAtIndex:indexPath.row] objectForKey:@"created"] ];
//    [cell.scopeOfWorksLabel setText:[[historyData objectAtIndex:indexPath.row] objectForKey:@"scope_of_work"] ];
//    [cell.contractorLabel setText:[[historyData objectAtIndex:indexPath.row] objectForKey:@"contractor"] ];
//    
//    NSURL *imageURL = [NSURL URLWithString:[[historyData objectAtIndex:0] objectForKey:@"image"] ];
//    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//    [cell.categoryImageView setImage:[UIImage imageWithData:imageData] ];
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"current_history"])
    {
        // Get reference to the destination view controller
        // Pass any objects to the view controller here, like...
        CurrentHistoryTableViewController *vc = [segue destinationViewController];
        [vc setSelectedRequestUuid:selectedRequestUuid];
    }
}
@end
