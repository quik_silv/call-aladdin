//
//  RateView.m
//  facclient
//
//  Created by mark on 9/18/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "RateView.h"

@implementation RateView
@synthesize rated;
@synthesize notSelectedImage;
@synthesize halfSelectedImage;
@synthesize fullSelectedImage;
@synthesize rating;
@synthesize editable;
@synthesize imageViews;
@synthesize maxRating;
@synthesize midMargin;
@synthesize leftMargin;
@synthesize minImageSize;
@synthesize delegate;

- (void)baseInit {
    rated = NO;
    notSelectedImage = nil;
    halfSelectedImage = nil;
    fullSelectedImage = nil;
    rating = 0;
    editable = NO;
    imageViews = [[NSMutableArray alloc] init];
    maxRating = 5;
    midMargin = 5;
    leftMargin = 0;
    minImageSize = CGSizeMake(5, 5);
    delegate = nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self baseInit];
    }
    return self;
}

- (void)refresh {
    for(int i = 0; i < self.imageViews.count; ++i) {
        UIImageView *imageView = [self.imageViews objectAtIndex:i];
        if (rating >= i+1) {
            [imageView setImage:fullSelectedImage];
        } else if (self.rating > i) {
            [imageView setImage:halfSelectedImage];
        } else {
            [imageView setImage:notSelectedImage];
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (notSelectedImage == nil) return;
    float desiredImageWidth = (self.frame.size.width - (leftMargin*2) - (midMargin*self.imageViews.count)) / imageViews.count;
    float imageWidth = MAX(minImageSize.width, desiredImageWidth);
    float imageHeight = MAX(minImageSize.height, self.frame.size.height);
    
    for (int i = 0; i < self.imageViews.count; ++i) {
        UIImageView *imageView = [imageViews objectAtIndex:i];
        CGRect imageFrame = CGRectMake(leftMargin + i*(midMargin+imageWidth), 0, imageWidth, imageHeight);
        imageView.frame = imageFrame;
    }
}

- (void)setMaxRating:(int)maxNumber {
    maxRating = maxNumber;
    
    // Remove old image views
    for(int i = 0; i < self.imageViews.count; ++i) {
        UIImageView *imageView = (UIImageView *) [self.imageViews objectAtIndex:i];
        [imageView removeFromSuperview];
    }
    [self.imageViews removeAllObjects];
    
    // Add new image views
    for(int i = 0; i < maxNumber; ++i) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.imageViews addObject:imageView];
        [self addSubview:imageView];
    }
    
    // Relayout and refresh
    [self setNeedsLayout];
    [self refresh];
}

- (void)setNotSelectedImage:(UIImage *)image {
    notSelectedImage = image;
    [self refresh];
}

- (void)setHalfSelectedImage:(UIImage *)image {
    halfSelectedImage = image;
    [self refresh];
}

- (void)setFullSelectedImage:(UIImage *)image {
    fullSelectedImage = image;
    [self refresh];
}

- (void)setRating:(float)number {
    rated = YES;
    rating = number;
    [self refresh];
}

- (void)handleTouchAtLocation:(CGPoint)touchLocation {
    if (!editable) return;
    
    int newRating = 0;
    for(int i = self.imageViews.count - 1; i >= 0; i--) {
        UIImageView *imageView = [self.imageViews objectAtIndex:i];
        if (touchLocation.x > imageView.frame.origin.x) {
            newRating = i+1;
            break;
        }
    }
    
    rating = newRating;
    [self refresh];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [delegate rateView:self ratingDidChange:rating];
}
@end
