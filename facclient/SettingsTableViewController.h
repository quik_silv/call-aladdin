//
//  SettingsTableViewController.h
//  facclient
//
//  Created by mark on 2017/10/06.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewController : UITableViewController
- (BOOL)signOut;
@end
