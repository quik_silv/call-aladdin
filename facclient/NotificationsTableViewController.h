//
//  NotificationsTableViewController.h
//  facclient
//
//  Created by mark on 2017/09/28.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsTableViewController : UITableViewController
@property (weak, nonatomic) NSString* selectedRequestUuid;
@property (strong, nonatomic) NSMutableDictionary *userInfo;
@end
