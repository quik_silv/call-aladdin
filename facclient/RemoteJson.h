//
//  RemoteJson.h
//  facclient
//
//  Created by mark on 11/19/16.
//  Copyright © 2016 Square Potato. All rights reserved.
//

#ifndef RemoteJson_h
#define RemoteJson_h
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface RemoteJson: NSObject
@property (strong, nonatomic) id responseJson;
@property long responseStatusCode;

- (NSArray *)updateDataFromRemoteSourceFrom:(NSString *)urlString;
- (bool) rateRequest:(NSDictionary *) queryData;
- (bool) flagFoundContractorRequest:(NSString *) requestUuid withContractor:(NSString *) contractorCompanyName;
- (bool) flagDeleteRequest:(NSString *) requestUuid;
- (bool) addRequest:(NSDictionary *) data withImage:(NSData *) imageData withVoice:(NSData *) voiceData;
- (bool) editRequest:(NSDictionary *) data withImage:(NSData *) imageData withVoice:(NSData *) voiceData;
- (bool) addIdentity:(NSDictionary *) data withImage:(NSData *) imageData;
- (bool) editIdentity:(NSDictionary *) identity withImage:(NSData *) imageData;
- (bool) flagDeleteIdentity:(NSString *) uuid;
- (NSString *)randomStringWithLength: (int) len;
@end
#endif
