//
//  RequestTableViewController.m
//  facclient
//
//  Created by mark on 12/30/16.
//  Copyright © 2016 Square Potato. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "RequestTableViewController.h"
#import "CitiesTableViewController.h"
#import "CountriesTableViewController.h"
#import "RemoteJson.h"
#import "Spinner.h"
#import "Services.h"
#import "MapViewController.h"
#import <SDWebImage/UIButton+WebCache.h>

@interface RequestTableViewController ()
{
    NSArray *queryData;
    NSArray *userProfileData;
    
    AVAudioPlayer *audioPlayer;
    AVAudioRecorder *audioRecorder;
    
    NSTimer* timer;
    int recordEncoding;
    enum {
        ENC_AAC = 1,
        ENC_ALAC = 2,
        ENC_IMA4 = 3,
        ENC_ILBC = 4,
        ENC_ULAW = 5,
        ENC_PCM = 6,
    } encodingTypes;
}
@end

@implementation RequestTableViewController
@synthesize selectedRequestUuid;
@synthesize cityName;
@synthesize countryName;
@synthesize longitude;
@synthesize latitude;

@synthesize overwriteRecordingLabel;
@synthesize microphoneButton;
@synthesize recordingLabel;
@synthesize addressLabel;
@synthesize addressTextField;
@synthesize workLabel;
@synthesize titleLabel;
@synthesize titleTextField;
@synthesize workTextField;
@synthesize photoButton;
@synthesize workCategoryLabel;
@synthesize workCategoryImageView;
@synthesize workCategory;
@synthesize preferredStartDate;
@synthesize preferredEndDate;
@synthesize workCategoryIconFilename;
@synthesize selectedCityLabel;
@synthesize selectedCountryLabel;
@synthesize currentTimeSlider;
@synthesize remainingTimeLabel;

- (void) reloadData {
    Spinner *spin = [[Spinner alloc] initWithView:self];
    [spin start];
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(selectedRequestUuid != NULL) {
            NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests/%@/", selectedRequestUuid];
            queryData = [[r updateDataFromRemoteSourceFrom:dataSourceUrl] objectAtIndex:0];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
            NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
            [dateFormatter setLocale:locale];
            [dateFormatter setTimeZone:timeZone];
            [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
            NSDate *startdate = [dateFormatter dateFromString:[queryData valueForKey:@"preferred_start_datetime"] ];
            
            NSDate *enddate = [dateFormatter dateFromString:[queryData valueForKey:@"preferred_end_datetime"] ];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            
            Services *s = [[Services alloc] init];
            NSArray *services = [s services];
            NSArray *servicesName = [s servicesName];
            NSURL *imageURL = [NSURL URLWithString:[queryData valueForKey:@"image"] ];
            [photoButton sd_setImageWithURL:imageURL forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"upload-from-images-placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
            
            NSString *workCategoryIconName = [[NSString stringWithFormat:@"%@", [queryData valueForKey:@"work_category"] ] lowercaseString];
            NSString *workCategoryIconFilename = [services objectAtIndex:[servicesName indexOfObject:workCategoryIconName] ];
            [workCategoryImageView setImage:[UIImage imageNamed:workCategoryIconFilename ] ];
            [addressTextField setText:[queryData valueForKey:@"address"]  ];
            [titleTextField setText:[queryData valueForKey:@"title"] ];
            [workTextField setText:[queryData valueForKey:@"scope_of_work"]  ];
            [preferredStartDate setText:[dateFormatter stringFromDate:startdate] ];
            [preferredEndDate setText:[dateFormatter stringFromDate:enddate] ];
            [selectedCityLabel setText:[queryData valueForKey:@"city"]  ];
            [selectedCountryLabel setText:[queryData valueForKey:@"country"]  ];
            [workCategoryLabel setText:[NSString stringWithFormat:@"%@", NSLocalizedString([queryData valueForKey:@"work_category"], nil) ] ];
            latitude = [[queryData valueForKey:@"latitude"] doubleValue];
            longitude = [[queryData valueForKey:@"longitude"] doubleValue];
        } else {
            //new request
            NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles?city=%@&work_categories=%@", [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"city"], workCategory];
            queryData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
            [workCategoryLabel setText:[NSString stringWithFormat:@"%@", NSLocalizedString(workCategory, nil) ] ];
            [workCategoryImageView setImage:[UIImage imageNamed:workCategoryIconFilename] ];
            if([queryData count] ==0) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
                                                                    message:NSLocalizedString(@"No contractors in this category for your city", nil)
                                                                   delegate:nil //self
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil];
                [alertView show];
            }
        }
        [spin stop];
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadData];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [selectedCityLabel setText:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"city"] ];
    [selectedCountryLabel setText:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"country"] ];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setLocale:locale];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    [preferredStartDate setText:[dateFormatter stringFromDate:[NSDate date] ] ];
    [preferredEndDate setText:[dateFormatter stringFromDate:[NSDate date] ] ];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];
    [preferredStartDate setReturnKeyType:UIReturnKeyDone];
    [preferredStartDate setInputView:datePicker];
    
    UIDatePicker *datePicker1 = [[UIDatePicker alloc] init];
    [datePicker1 setDatePickerMode:UIDatePickerModeDate];
    [datePicker1 addTarget:self action:@selector(updateTextField1:)
          forControlEvents:UIControlEventValueChanged];
    [preferredEndDate setInputView:datePicker1];
    
    //set all UITextField delegate to self
    titleTextField.delegate = (id)self;
    workTextField.delegate = (id)self;
    preferredStartDate.delegate = (id)self;
    preferredEndDate.delegate = (id)self;
    addressTextField.delegate = (id)self;
}

-(void)updateTextField:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    [dateFormatter setLocale:locale];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [preferredStartDate setText:[dateFormatter stringFromDate:sender.date] ];
}
-(void)updateTextField1:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    [dateFormatter setLocale:locale];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [preferredEndDate setText:[dateFormatter stringFromDate:sender.date] ];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if ([[segue identifier] isEqualToString:@"selectGeoCoordinatesForRequest"])
     {
         // Get reference to the destination view controller
         // Pass any objects to the view controller here, like...
         MapViewController *vc = [segue destinationViewController];
         [vc setEnableAnnotation:YES];
         [vc setLocationCoordinate:CLLocationCoordinate2DMake(latitude, longitude) ] ;
     }
 }
 

/*
 - (void)embedSpinnerIn: (UIView *)currentView {
 //set ivar in your view controller
 //UIActivityIndicatorView * spinner;
 
 //alloc init it  in the viewdidload
 spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
 [currentView addSubview:spinner];
 [spinner setFrame:CGRectMake(0, 0, 100, 100)];
 [spinner setCenter:CGPointMake(currentView.center.x, 150)];
 spinner.transform = CGAffineTransformMakeScale(2, 2);
 [spinner setColor:[UIColor darkGrayColor]];
 }
 */

- (IBAction)submitButton:(id)sender {
    if (selectedRequestUuid != NULL) {
        [self updateRequest];
    } else {
        [self createRequest];
    }
}
- (void) createRequest {
    RemoteJson *r = [[RemoteJson alloc] init];
    NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles?uuid=%@", [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"uuid"] ];
    userProfileData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
    if ([userProfileData count] == 0) {
        //probably coming directly from UserRegistrationViewController and the data has not been saved into the database due to network conditions, proper way to do it would be from RemoteJson and continue only when the data is correctly sent. Here is a workaround because I have not much time.
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil)
                                                            message:NSLocalizedString(@"Please register with us.", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *tabController = [storyboard instantiateViewControllerWithIdentifier:@"WelcomeViewIdentifier"];
        [self presentViewController:tabController animated:YES completion:Nil];
    } else {
        NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests?requestor=%@&status=pending&work_category=%@&modified=1", [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"uuid"], workCategory];
        queryData = [[NSMutableArray alloc] initWithArray:[r updateDataFromRemoteSourceFrom:dataSourceUrl] ];
        if([queryData count] > 3) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
                                                                message:NSLocalizedString(@"You are limited to three queries per month to the same category", nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                      otherButtonTitles:nil];
            [alertView show];
        } else {
            UIColor *defaultColor = [UIColor blackColor];
            CGFloat defaultTextFieldBorderWidth = 0.0f;
            
            addressLabel.textColor = defaultColor;
            workLabel.textColor = defaultColor;
            
            addressTextField.layer.borderWidth = defaultTextFieldBorderWidth;
            workTextField.layer.borderWidth = defaultTextFieldBorderWidth;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
            NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
            [dateFormatter setLocale:locale];
            [dateFormatter setTimeZone:timeZone];
            [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            
            BOOL empty = NO;
            if ([[titleTextField text] length] == 0) {
                titleLabel.textColor = [UIColor redColor];
                titleTextField.layer.borderColor = [[UIColor redColor] CGColor];
                titleTextField.layer.borderWidth = 1;
                empty = YES;
            }
            if ([[addressTextField text] length] == 0) {
                addressLabel.textColor = [UIColor redColor];
                addressTextField.layer.borderColor = [[UIColor redColor] CGColor];
                addressTextField.layer.borderWidth = 1;
                empty = YES;
            }
            if ([[workTextField text] length] == 0) {
                workLabel.textColor = [UIColor redColor];
                workTextField.layer.borderColor = [[UIColor redColor] CGColor];
                workTextField.layer.borderWidth = 1;
                empty = YES;
            }
            if ([[preferredStartDate text] length] ==0) {
                [preferredStartDate setText:[dateFormatter stringFromDate:[NSDate date] ] ];
            }
            if ([[preferredEndDate text] length] ==0) {
                [preferredEndDate setText:[dateFormatter stringFromDate:[NSDate date] ] ];
            }
            //need to make sure no nulls are entered into NSDictionary
            if(!empty) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
                [dateFormatter setLocale:locale];
                [dateFormatter setTimeZone:timeZone];
                [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
                
                [dateFormatter setDateFormat:@"dd MMM yyyy"];
                NSDate *startdate = [dateFormatter dateFromString:[preferredStartDate text] ];
                NSDate *enddate = [dateFormatter dateFromString:[preferredEndDate text] ];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
                NSDictionary *client = @{
                                         @"requestor":[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"uuid"],
                                         @"title":[titleTextField text],
                                         @"scope_of_work":[workTextField text],
                                         @"address":[addressTextField text],
                                         @"longitude":@(longitude),
                                         @"latitude":@(latitude),
                                         @"city":[selectedCityLabel text],
                                         @"country":[selectedCountryLabel text],
                                         @"contractor":@"", //when pending, no contractor assigned
                                         @"preferred_start_datetime":[dateFormatter stringFromDate:startdate],
                                         @"preferred_end_datetime":[dateFormatter stringFromDate:enddate],
                                         @"work_category":workCategory,
                                         };
                Spinner *spin = [[Spinner alloc] initWithView:self];
                [spin start];
                RemoteJson *r = [[RemoteJson alloc] init];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *recordedAudioPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/voicenote.caf"];
                    NSURL *voiceUrl = [NSURL fileURLWithPath:[NSString stringWithString:recordedAudioPath] ];
                    NSData *voiceFile = [NSData dataWithContentsOfURL:voiceUrl];
                    [r addRequest:client withImage:UIImagePNGRepresentation([[photoButton imageView] image]) withVoice:voiceFile];
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Job submitted", nil)
                                                                        message:NSLocalizedString(@"Your job has been posted to available contractors in town. Please verify contractors quality of work before finalising.", nil)
                                                                       delegate:self
                                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                              otherButtonTitles:nil];
                    [alertView show];
                    [spin stop];
                });
            }
        }
    }
}
- (void) updateRequest {
    RemoteJson *r = [[RemoteJson alloc] init];
    NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests?status=pending&work_category=%@&modified=1", [workCategoryLabel text]];
    queryData = [[NSMutableArray alloc] initWithArray:[r updateDataFromRemoteSourceFrom:dataSourceUrl] ];
    if([queryData count] > 2) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
                                                            message:NSLocalizedString(@"You are limited to three queries per month to the same category.", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
    } else {
        UIColor *defaultColor = [UIColor blackColor];
        CGFloat defaultTextFieldBorderWidth = 0.0f;
        
        addressLabel.textColor = defaultColor;
        workLabel.textColor = defaultColor;
        
        addressTextField.layer.borderWidth = defaultTextFieldBorderWidth;
        workTextField.layer.borderWidth = defaultTextFieldBorderWidth;
        
        BOOL empty = NO;
        if ([[addressTextField text] length] == 0) {
            addressLabel.textColor = [UIColor redColor];
            addressTextField.layer.borderColor = [[UIColor redColor] CGColor];
            addressTextField.layer.borderWidth = 1;
            empty = YES;
        }
        if ([[workTextField text] length] == 0) {
            workLabel.textColor = [UIColor redColor];
            workTextField.layer.borderColor = [[UIColor redColor] CGColor];
            workTextField.layer.borderWidth = 1;
            empty = YES;
        }
        if(!empty) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
            NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
            [dateFormatter setLocale:locale];
            [dateFormatter setTimeZone:timeZone];
            [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
            
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            NSDate *startdate = [dateFormatter dateFromString:[preferredStartDate text] ];
            NSDate *enddate = [dateFormatter dateFromString:[preferredEndDate text] ];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
            NSDictionary *client = @{
                                     @"uuid":selectedRequestUuid,
                                     @"requestor_uuid":[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"identifier_for_vendor"],
                                     @"title":[titleTextField text],
                                     @"scope_of_work":[workTextField text],
                                     @"address":[addressTextField text],
                                     @"longitude":@(longitude),
                                     @"latitude":@(latitude),
                                     @"work_category":[workCategoryLabel text],
                                     @"city":[selectedCityLabel text],
                                     @"country":[selectedCountryLabel text],
                                     @"contractor":@"",
                                     @"preferred_start_datetime":[dateFormatter stringFromDate:startdate],
                                     @"preferred_end_datetime":[dateFormatter stringFromDate:enddate],
                                     };
            Spinner *spin = [[Spinner alloc] initWithView:self];
            [spin start];
            RemoteJson *r = [[RemoteJson alloc] init];
            dispatch_async(dispatch_get_main_queue(), ^{
//                NSString *recordedAudioPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/voicenote.caf"];
//                NSURL *voiceUrl = [NSURL fileURLWithPath:[NSString stringWithString:recordedAudioPath] ];
//                NSData *voiceFile = [NSData dataWithContentsOfURL:voiceUrl];
//                [r editRequest:client withImage:UIImagePNGRepresentation([[photoButton imageView] image]) withVoice:voiceFile];
                [r editRequest:client withImage:UIImagePNGRepresentation([[photoButton imageView] image]) withVoice:NULL];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
                                                                    message:NSLocalizedString(@"Request updated.", nil)
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil];
                [alertView show];
                [spin stop];
            });
        }
    }
}

- (IBAction)startRecord:(id)sender {
    [recordingLabel setHidden:NO];
    // Init audio with record capability
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:nil];
    
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] initWithCapacity:10];
    if(recordEncoding == ENC_PCM)
    {
        [recordSettings setObject:[NSNumber numberWithInt: kAudioFormatLinearPCM] forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
    }
    else
    {
        NSNumber *formatObject;
        
        switch (recordEncoding) {
            case (ENC_AAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatMPEG4AAC];
                break;
            case (ENC_ALAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleLossless];
                break;
            case (ENC_IMA4):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleIMA4];
                break;
            case (ENC_ILBC):
                formatObject = [NSNumber numberWithInt: kAudioFormatiLBC];
                break;
            case (ENC_ULAW):
                formatObject = [NSNumber numberWithInt: kAudioFormatULaw];
                break;
            default:
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleIMA4];
        }
        
        [recordSettings setObject:formatObject forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:12800] forKey:AVEncoderBitRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithInt: AVAudioQualityHigh] forKey: AVEncoderAudioQualityKey];
    }
        NSString *recordedAudioPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/voicenote.caf"];
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithString:recordedAudioPath] ];
    
        NSError *error = nil;
        audioRecorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSettings error:&error];
        
        if ([audioRecorder prepareToRecord] == YES){
            [audioRecorder record];
        }else {
            NSInteger errorCode = CFSwapInt32HostToBig ([error code]);
            NSLog(@"Error: %@ [%4.4s])" , [error localizedDescription], (char*)&errorCode);
        }
}
- (IBAction)stopRecord:(id)sender {
    [recordingLabel setHidden:YES];
    [audioRecorder stop];
    
    [overwriteRecordingLabel setHidden:NO];
    [overwriteRecordingLabel setEnabled:YES];
    
    NSString *recordedAudioPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/voicenote.caf"];
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithString:recordedAudioPath] ];
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    [remainingTimeLabel setText:[NSString stringWithFormat:@"%.02f s", audioPlayer.duration] ];
    
//    [microphoneButton setHidden:YES];
//    [microphoneButton setEnabled:NO];
//    [speakerButton setHidden:NO];
//    [speakerButton setEnabled:YES];
    [self updateDisplay];
}
- (IBAction)startPlay:(id)sender {
    // Init audio with playback capability
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSString *recordedAudioPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/voicenote.caf"];
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithString:recordedAudioPath] ];
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer play];
    [currentTimeSlider setMaximumValue:audioPlayer.duration];
    timer = [NSTimer
                  scheduledTimerWithTimeInterval:0.1
                  target:self selector:@selector(timerFired:)
                  userInfo:nil repeats:YES];
}
-(void)timerFired:(NSTimer*) timer {
    [self updateDisplay];
}
-(void)stopTimer {
    [timer invalidate];
    timer = nil;
}
-(IBAction)stopPlay:(id)sender {
    [audioPlayer stop];
    [self stopTimer];
    [self updateDisplay];
}
#pragma mark - Display Update
- (void)updateDisplay
{
    NSTimeInterval currentTime = audioPlayer.currentTime;
    NSString* currentTimeString = [NSString stringWithFormat:@"%.02f", currentTime];
    
    currentTimeSlider.value = currentTime;
    [self updateSliderLabels];
    
//    self.currentTimeLabel.text = currentTimeString;
//    self.playingLabel.text = self.player.playing ? @"YES" : @"NO";
//    self.deviceCurrentTimeLabel.text = [NSString stringWithFormat:@"%.02f", self.player.deviceCurrentTime];
}

- (void)updateSliderLabels
{
    NSTimeInterval currentTime = currentTimeSlider.value;
    NSString* currentTimeString = [NSString stringWithFormat:@"%.02f", currentTime];
    
    //self.elapsedTimeLabel.text =  currentTimeString;
    [remainingTimeLabel setText:[NSString stringWithFormat:@"%.02f s", audioPlayer.duration - currentTime] ];
}
- (IBAction)currentTimeSliderValueChanged:(id)sender
{
    if(timer)
        [self stopTimer];
    [self updateSliderLabels];
}
- (IBAction)currentTimeSliderTouchUpInside:(id)sender
{
    [audioPlayer stop];
    audioPlayer.currentTime = currentTimeSlider.value;
    [audioPlayer prepareToPlay];
    [self startPlay:self];
}
//- (IBAction)overwriteRecording:(id)sender {
//    [audioPlayer stop];
//    [audioRecorder stop];
//
//    [overwriteRecordingLabel setHidden:YES];
//    [overwriteRecordingLabel setEnabled:NO];
//
//    [microphoneButton setHidden:NO];
//    [microphoneButton setEnabled:YES];
//    [speakerButton setHidden:YES];
//    [speakerButton setEnabled:NO];
//
//}

#pragma mark - AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"%s successfully=%@", __PRETTY_FUNCTION__, flag ? @"YES" : @"NO");
    [self stopTimer];
    [self updateDisplay];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"%s error=%@", __PRETTY_FUNCTION__, error);
    [self stopTimer];
    [self updateDisplay];
}

- (IBAction)takePhoto:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    [actionSheet setDelegate:(id)self];
    [actionSheet setTitle:@"Photos"];
    [actionSheet addButtonWithTitle:@"Camera"];
    [actionSheet addButtonWithTitle:@"Library"];
    [actionSheet showInView:self.tableView];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    if(buttonIndex == 0) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else if(buttonIndex == 1) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        UIViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
        //        [self.navigationController pushViewController:controller animated:YES];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //    [photoImageView setImage:chosenImage];
    [photoButton setImage:chosenImage forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)setSelectedService:(NSString*)name {
    workCategory = name;
}
- (void)setSelectedServiceIcon:(NSString*)filename {
    workCategoryIconFilename = filename;
}
- (void)setSelectedCity:(NSString *)name {
    cityName = name;
}

- (void)setSelectedCountry:(NSString*)name {
    countryName = name;
}

-(IBAction)unwindFromCitySelectionToRequestCreation:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[CitiesTableViewController class]]) {
        [selectedCityLabel setText:cityName];
    }
}

-(IBAction)unwindFromCountrySelectionToRequestCreation:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[CountriesTableViewController class]]) {
        [selectedCountryLabel setText:countryName];
    }
}
-(IBAction)unwindFromSelectJobCoordinate:(UIStoryboardSegue *)unwindSegue {
    NSLog(@"test: %@ %@ %@", [unwindSegue.sourceViewController class], @(latitude), @(longitude) );
}
@end
