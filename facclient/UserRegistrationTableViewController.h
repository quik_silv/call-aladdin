//
//  UserRegistrationTableViewController.h
//  facclient
//
//  Created by mark on 5/7/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserRegistrationTableViewController : UITableViewController<UIActionSheetDelegate>

- (void)verifyUser;
- (void)registerUser;
- (IBAction)takePhoto:(id)sender;
@property NSString *authVerificationCode;
@property (weak, nonatomic) IBOutlet UIButton *takePhoto;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
- (IBAction)toggleContractorSwitch:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *contractorSwitch;
@property (weak, nonatomic) NSString* cityName;
@property (weak, nonatomic) IBOutlet UILabel *selectedCityLabel;
- (void)setSelectedCity:(NSString*)name;
@property (weak, nonatomic) NSString* countryName;
@property (weak, nonatomic) IBOutlet UILabel *selectedCountryLabel;
- (void)setSelectedCountry:(NSString*)name;
@property (weak, nonatomic) IBOutlet UILabel *workCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedWorkCategory;
@property (strong, nonatomic) NSArray* selectedServices;
@property (weak, nonatomic) IBOutlet UILabel *companyRegisteredAddressLabel;
@property (weak, nonatomic) IBOutlet UITextField *companyRegisteredAddressTextField;
@property (weak, nonatomic) IBOutlet UILabel *yearsOfExperienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *companyNameTextField;
- (IBAction)doneButton:(id)sender;
@end
