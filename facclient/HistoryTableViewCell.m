//
//  HistoryTableViewCell.m
//  facclient
//
//  Created by mark on 8/5/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "HistoryTableViewCell.h"

@implementation HistoryTableViewCell
@synthesize createdLabel;
@synthesize workCategoryLabel;
@synthesize statusLabel;
@synthesize contractorLabel;
@synthesize contractorImageView;
@synthesize rateLabel;
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [[rateLabel layer] setBorderWidth:1.5f];
    [[rateLabel layer] setCornerRadius:5];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    //[userRatingLabel setText:[[NSNumber numberWithFloat:rating] stringValue] ];
}
@end
