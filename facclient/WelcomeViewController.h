//
//  WelcomeViewController.h
//  facclient
//
//  Created by mark on 3/14/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
- (IBAction)userLogin:(id)sender;
@end
