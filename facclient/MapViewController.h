//
//  MapViewController.h
//  facclient
//
//  Created by Mark Wong on 2018/07/31.
//  Copyright © 2018 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate>
- (IBAction)CoordinateSelected:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property BOOL enableAnnotation;
@property CLLocationCoordinate2D locationCoordinate;
@end
