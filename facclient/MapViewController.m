//
//  MapViewController.m
//  facclient
//
//  Created by Mark Wong on 2018/07/31.
//  Copyright © 2018 Square Potato. All rights reserved.
//

#import "MapViewController.h"
#import "RequestTableViewController.h"

@interface MapViewController ()
{
    MKPointAnnotation *annotationPoint;
    CLLocationManager *locationManager;
}
@end

@implementation MapViewController
@synthesize mapView;
@synthesize locationCoordinate;
@synthesize enableAnnotation;
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // Create an instance of CLLocation
//    CLLocation *location=[locationManager location];
    // Set Center Coordinates of MapView
//    mapView.centerCoordinate=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    if(!locationCoordinate.latitude || !locationCoordinate.longitude) {
        locationManager=[[CLLocationManager alloc] init];
        [locationManager requestWhenInUseAuthorization];
        [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        locationManager.delegate=(id)self;
        [locationManager startUpdatingLocation];
        CLLocation *location=[locationManager location];
        locationCoordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    }
    mapView.centerCoordinate=locationCoordinate;
    // Set Annotation to show current Location
    annotationPoint=[[MKPointAnnotation alloc] init];
    annotationPoint.coordinate=mapView.centerCoordinate;
    [mapView addAnnotation:annotationPoint];
    // Setting Zoom Level on MapView
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(locationCoordinate, 1000, 1000);
    MKCoordinateRegion adjustedRegion = [mapView regionThatFits:viewRegion];
    adjustedRegion.center = mapView.centerCoordinate;
    [mapView setRegion:adjustedRegion animated:YES];
    
    // Show userLocation (Blue Circle)
    mapView.showsUserLocation=YES;
}
//
//-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
//{
//    latitude=userLocation.coordinate.latitude;
//    longitude=userLocation.coordinate.longitude;
//
//    mapView.centerCoordinate=CLLocationCoordinate2DMake(latitude, longitude);
//
//    MKPointAnnotation *annotationPoint=[[MKPointAnnotation alloc] init];
//    annotationPoint.coordinate=mapView.centerCoordinate;
//    annotationPoint.title=@"Moradabad";
//    annotationPoint.subtitle=@"My Home Town";
//}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(handleTap:)];
    [tapGesture setCancelsTouchesInView:NO];
    [tapGesture setDelaysTouchesEnded:NO];
    [mapView addGestureRecognizer:tapGesture];
    tapGesture.delegate = (id)self;
}

- (void)handleTap:(id)sender
{
    UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer*)sender;
    CGPoint tapPoint = [tapGesture locationInView:mapView];
    CLLocationCoordinate2D coord = [mapView convertPoint:tapPoint toCoordinateFromView:mapView];
    
//    NSUInteger numberOfTouches = [tapGesture numberOfTouches];
    if(enableAnnotation) {
        [annotationPoint setCoordinate:coord];
        [mapView addAnnotation:annotationPoint];
    }
//    if (numberOfTouches == 1) {
//        NSLog(@"Tap location was %.0f, %.0f", tapPoint.x, tapPoint.y);
//        NSLog(@"World coordinate was longitude %f, latitude %f", coord.longitude, coord.latitude);
//    } else {
//        NSLog(@"Number of touches was %d, ignoring", numberOfTouches);
//    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:
(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"selectJobCoordinates"])
    {
        RequestTableViewController *destViewController = [segue destinationViewController];
        [destViewController setLongitude:[annotationPoint coordinate].longitude];
        [destViewController setLatitude:[annotationPoint coordinate].latitude];
    }
}
- (IBAction)CoordinateSelected:(id)sender {
    [self performSegueWithIdentifier:@"selectJobCoordinates" sender:nil];
}
@end
