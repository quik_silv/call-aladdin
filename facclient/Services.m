#import <Foundation/Foundation.h>
#import "Services.h"

@interface Services ()

@end

@implementation Services
@synthesize services;
@synthesize servicesName;
- (Services *) init {
services = [NSArray arrayWithObjects:@"concrete-01", @"bulb-01", @"cabinet-01", @"wood-01", @"curtain-01", @"signboard-01", @"aircond-01", @"ceiling-01", @"window-01", @"cctv-01", @"vacuum-01", @"tree-01", @"gate-01", @"water tap-01", @"termite-01", @"fengshui-01", @"generalworkers-01", @"housemovers-01", nil];
servicesName = [NSArray arrayWithObjects:
                @"construction, tiling & painting",
                @"wiring & lighting",
                @"interior design & carpenters",
                @"laminated, timber flooring",
                @"curtain, carpet & wallpaper",
                @"signboard",
                @"air conditioner",
                @"plastered ceiling",
                @"aluminium & glass works",
                @"alarm & cctv",
                @"cleaning services",
                @"landscaping & pond",
                @"gates & steel works",
                @"plumber",
                @"pest control",
                @"fengshui consultation",
                @"general workers",
                @"house movers",
                nil];
    return self;
}

- (NSDate *) ConvertDate:(NSString *) fromString :(NSString *) withFormat   {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setLocale:locale];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:withFormat];
    return [dateFormatter dateFromString:fromString];
}
@end
