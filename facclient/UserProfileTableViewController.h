//
//  UserProfileTableViewController.h
//  facclient
//
//  Created by mark on 8/29/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileTableViewController : UITableViewController
- (IBAction)takePhoto:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
- (IBAction)toggleContractorSwitch:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *contractorSwitch;
@property (weak, nonatomic) NSString* cityName;
@property (weak, nonatomic) IBOutlet UILabel *selectedCityLabel;
- (void)setSelectedCity:(NSString*)name;
@property (weak, nonatomic) NSString* countryName;
@property (weak, nonatomic) IBOutlet UILabel *selectedCountryLabel;
- (void)setSelectedCountry:(NSString*)name;
@property (weak, nonatomic) IBOutlet UILabel *workCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedWorkCategoryLabel;
@property (strong, nonatomic) NSArray *selectedServices;
//- (void)setSelectedServices:(NSMutableArray *)array;
@property (weak, nonatomic) IBOutlet UILabel *companyRegisteredAddressLabel;
@property (weak, nonatomic) IBOutlet UITextField *companyRegisteredAddressTextField;
@property (weak, nonatomic) IBOutlet UILabel *yearsOfExperienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *companyNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *passwordRegisteredLabel;

- (IBAction)editUser:(id)sender;
@end
