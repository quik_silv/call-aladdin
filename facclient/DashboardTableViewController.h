//
//  DashboardTableViewController.h
//  facclient
//
//  Created by mark on 8/17/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardTableViewController : UITableViewController
@property (weak, nonatomic) NSString* selectedRequestUuid;
@end