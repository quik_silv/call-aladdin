//
//  Spinner.h
//  facclient
//
//  Created by mark on 3/17/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Spinner: UIView
- (UIView*)initWithView: (UIViewController *)parentViewController;
- (void)start;
- (void)stop;
@end
