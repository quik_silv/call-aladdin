//
//  NotificationsTableViewController.m
//  facclient
//
//  Created by mark on 2017/09/28.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "RemoteJson.h"
#import "Spinner.h"
#import "NotificationsTableViewController.h"
#import "CurrentNotificationTableViewController.h"
#import "DashboardTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface NotificationsTableViewController ()
{
    NSArray *queryData;
}
@end

@implementation NotificationsTableViewController
@synthesize selectedRequestUuid;
@synthesize userInfo;

- (void)reloadData {
    Spinner *spin = [[Spinner alloc] initWithView:self];
    [spin start];
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"is_contractor"] intValue]) {
            NSMutableString *workCategories = [NSMutableString stringWithString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"work_categories"] ];
            NSString *city = [NSMutableString stringWithString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"city"] ];
            NSString *country = [NSMutableString stringWithString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"country"] ];
            NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests?status=pending&work_category=%@&city=%@&country=%@", workCategories, city, country];
            queryData = [[NSMutableArray alloc] initWithArray:[r updateDataFromRemoteSourceFrom:dataSourceUrl] ];
        }
        if (![queryData count]) {
//            [[self tableView] reloadData];
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
//                                                                message:NSLocalizedString(@"There are no pending job requests", nil)
//                                                               delegate:self
//                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
//                                                      otherButtonTitles:nil];
//            [alertView show];
        } else {
            [[self tableView] reloadData];
        }
        [spin stop];
    });
}

- (void)getLatestRequests:(UIRefreshControl *)refreshControl {
    [self reloadData];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *updated = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:updated];
    [self.refreshControl endRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.navigationItem setHidesBackButton:YES];
//    [self reloadData];
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor orangeColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestRequests:)
                  forControlEvents:UIControlEventValueChanged];
    NSLog(@"%@", [[userInfo objectForKey:@"aps"] objectForKey:@"badge"]);
    if ([[userInfo objectForKey:@"aps"] objectForKey:@"badge"]) {
        [[self navigationController] tabBarItem].badgeValue = [NSString stringWithFormat:@"%@", [[userInfo objectForKey:@"aps"] objectForKey:@"badge"] ];
    }
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadData];
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//    [[[[[self tabBarController] viewControllers] objectAtIndex:2] tabBarItem] setBadgeValue:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger numberOfSections = 0;
    if ([queryData count])
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numberOfSections = 1;
        self.tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
        noDataLabel.text             = NSLocalizedString(@"There are no pending job requests", nil);
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        self.tableView.backgroundView = noDataLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [queryData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 115;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DashboardTableViewCell *cell = (DashboardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"dashboardCell"];
    if(cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"DashboardTableViewCell" bundle:nil] forCellReuseIdentifier:@"dashboardCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"dashboardCell" forIndexPath:indexPath];
    }
    [cell.titleLabel setFont:[UIFont systemFontOfSize:17.] ];
    [cell.preferredDateRangeLabel setFont:[UIFont systemFontOfSize:13.] ];
    [cell.scopeOfWorksLabel setFont:[UIFont systemFontOfSize:17.] ];
    if ([[[userInfo objectForKey:@"aps"] objectForKey:@"thread-id"] isEqualToString:[[queryData objectAtIndex:indexPath.row] objectForKey:@"uuid"] ]) {
        //if notification is new and unread
        [cell.titleLabel setFont:[UIFont boldSystemFontOfSize:18.] ];
        [cell.preferredDateRangeLabel setFont:[UIFont systemFontOfSize:14.] ];
        [cell.scopeOfWorksLabel setFont:[UIFont boldSystemFontOfSize:18.] ];
    }
    [cell.titleLabel setText:[[queryData objectAtIndex:indexPath.row] objectForKey:@"title"] ];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale        *locale         = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSTimeZone      *timeZone       = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setLocale:locale];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *startdate = [dateFormatter dateFromString:[[queryData objectAtIndex:indexPath.row] objectForKey:@"preferred_start_datetime"] ];
    
    NSDate *enddate = [dateFormatter dateFromString:[[queryData objectAtIndex:indexPath.row] objectForKey:@"preferred_end_datetime"] ];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    
    [cell.preferredDateRangeLabel setText:[NSString stringWithFormat:@"%@ to %@", [dateFormatter stringFromDate:startdate], [dateFormatter stringFromDate:enddate] ] ];
    [cell.scopeOfWorksLabel setText:[[queryData objectAtIndex:indexPath.row] objectForKey:@"scope_of_work"] ];
    
    NSURL *imageURL = [NSURL URLWithString:[[queryData objectAtIndex:indexPath.row] objectForKey:@"image"] ];
    [cell.categoryImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"upload-from-images-placeholder.png"] options:SDWebImageRefreshCached];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [userInfo removeAllObjects];
    selectedRequestUuid = [[queryData objectAtIndex:indexPath.row] objectForKey:@"uuid"];
    [self performSegueWithIdentifier:@"current_notifications" sender:self];
}

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"current_notifications"])
    {
        [[userInfo objectForKey:@"aps"] setValue:@"0" forKey:@"badge"];
        [[self navigationController] tabBarItem].badgeValue = nil;
        // Get reference to the destination view controller
        // Pass any objects to the view controller here, like...
        CurrentNotificationTableViewController *vc = [segue destinationViewController];
        [vc setSelectedRequestUuid:selectedRequestUuid];
    }
}

@end
