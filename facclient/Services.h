#ifndef Services_h
#define Services_h
#import <Foundation/Foundation.h>

@interface Services: NSObject
@property (weak, nonatomic) NSArray *services;
@property (weak, nonatomic) NSArray *servicesName;

- (NSDate *) ConvertDate:(NSString *) fromString :(NSString *) withFormat;
@end
#endif
