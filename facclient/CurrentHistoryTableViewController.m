//
//  CurrentHistoryTableViewController.m
//  facclient
//
//  Created by mark on 9/17/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "CurrentHistoryTableViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "RemoteJson.h"
#import "Spinner.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CurrentHistoryTableViewController ()
{
    NSArray *queryData;
    NSString *placeholderText;
    NSString *youNeedToTellUsWhyText;
}
@end

@implementation CurrentHistoryTableViewController
@synthesize selectedRequestUuid;
@synthesize contractorLabel;
@synthesize photoImageView;
@synthesize rateView;
@synthesize userRating;
@synthesize commentTextView;
@synthesize rateButton;

- (void)reloadData {
    [rateButton setEnabled:NO];
    Spinner *spin = [[Spinner alloc] initWithView:self];
    [spin start];
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        placeholderText = NSLocalizedString(@"Your comments please...", nil);
        youNeedToTellUsWhyText = NSLocalizedString(@"You need to tell us why...", nil);
        NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests/%@/", selectedRequestUuid];
        queryData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
        if ([queryData count]) {
            if([[queryData valueForKey:@"contractor"] isEqual:[NSNull null] ]) {
                //just to be safe
                [contractorLabel setText:@""];
            } else {
//                NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles/%@/", [[queryData valueForKey:@"contractor"] objectForKey:@"uuid"] ];
//                NSArray *contractorData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
//
//                NSMutableDictionary *modifiedDictionary = [queryData mutableCopy];
//                [modifiedDictionary setValue:[contractorData valueForKey:@"company_name"]  forKey:@"contractor_company_name"];
//                [modifiedDictionary setValue:[contractorData valueForKey:@"image"] forKey:@"contractor_image"];
//
//                queryData = [modifiedDictionary copy];
                [contractorLabel setText:[[[queryData objectAtIndex:0] objectForKey:@"contractor"] objectForKey:@"company_name"] ];
                NSURL *imageURL = [NSURL URLWithString:[[[queryData objectAtIndex:0] objectForKey:@"contractor"] objectForKey:@"image"] ];
                [photoImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"upload-from-images-placeholder.png"] ];

                [rateView setNotSelectedImage:[UIImage imageNamed:@"aladdin-app-star-empty.png"]];
                [rateView setFullSelectedImage:[UIImage imageNamed:@"aladdin-app-star.png"]];
                [rateView setMaxRating:5];
                [rateView setDelegate:self];

                [rateView setRating:0];
                [rateView setEditable:YES];
                [[commentTextView layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
                [[commentTextView layer] setBorderWidth:1.0f];
                [[commentTextView layer] setCornerRadius:10];
                [commentTextView setDelegate:self];
                [commentTextView setText:placeholderText];
                [commentTextView setEditable:YES];
                
                [commentTextView setTextColor:[UIColor lightGrayColor] ];
                if ([[queryData objectAtIndex:0] valueForKey:@"rating"]) {
                    [rateView setRating:[[[queryData objectAtIndex:0] valueForKey:@"rating"] floatValue] ];
                    [rateView setEditable:NO]; //previously rated so disable
                    
                    [commentTextView setEditable:NO];
                    [commentTextView setText:[[queryData objectAtIndex:0] valueForKey:@"comment"] ];
                    [commentTextView setTextColor:[UIColor blackColor] ];
                    
                    [rateButton setEnabled:NO];
                    [rateButton setHidden:YES];
                }
            }
        } else {
            //no data, but if this happens, its an error
        }
        [spin stop];
    });
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadData];
}

- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    [rateButton setEnabled:NO];
    userRating = rating;
    if (userRating < 3.0f) {
        if([commentTextView.text isEqualToString:@""] || [commentTextView.text isEqualToString:placeholderText] || [commentTextView.text isEqualToString:youNeedToTellUsWhyText]) {
            [commentTextView setText:youNeedToTellUsWhyText];
            [commentTextView setTextColor:[UIColor lightGrayColor] ];
        } else {
            [rateButton setEnabled:YES];
        }
        [[commentTextView layer] setBorderColor:[[UIColor redColor] CGColor] ];
    } else {
        if([commentTextView.text isEqualToString:@""] || [commentTextView.text isEqualToString:placeholderText] || [commentTextView.text isEqualToString:youNeedToTellUsWhyText]) {
            [commentTextView setText:placeholderText];
            [commentTextView setTextColor:[UIColor lightGrayColor] ];
        }
        [[commentTextView layer] setBorderColor:[[UIColor grayColor] CGColor] ];
        [rateButton setEnabled:YES];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [rateButton setEnabled:NO];
    if ([textView.text isEqualToString:placeholderText] || [textView.text isEqualToString:youNeedToTellUsWhyText]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""] || [textView.text isEqualToString:placeholderText] || [textView.text isEqualToString:youNeedToTellUsWhyText]) {
        [textView setText:placeholderText];
        textView.textColor = [UIColor lightGrayColor]; //optional
        [rateButton setEnabled:NO];
    } else {
        [rateButton setEnabled:YES];
    }
    [textView resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *tabController = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
        [self presentViewController:tabController animated:YES completion:Nil];
    }
}

- (IBAction)rateRequest:(id)sender {
    if(([[commentTextView text] isEqualToString:placeholderText] || [[commentTextView text] isEqual:[NSNull null] ]) && userRating < 3.0f) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
                                                            message:youNeedToTellUsWhyText
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
    } else {
//        NSString *comments = @"";
//        if(![[commentTextView text] isEqualToString:placeholderText] || ![[commentTextView text] isEqualToString:youNeedToTellUsWhyText]) {
//            comments = [commentTextView text];
//        }
        NSString *comments = [commentTextView text];
        NSDictionary *ratingData = @{
                                     @"uuid":selectedRequestUuid,
                                     @"rating":[NSNumber numberWithFloat:userRating],
                                     @"comment":comments,
                                     @"status":@"completed"
                                     };
        RemoteJson *r = [[RemoteJson alloc] init];
        [r rateRequest:ratingData];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
                                                            message:NSLocalizedString(@"Thank you for rating", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}
@end
