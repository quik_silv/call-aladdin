//
//  JobCategoriesCollectionViewController.h
//  
//
//  Created by mark on 2017/09/27.
//

#import <UIKit/UIKit.h>

@interface JobCategoriesCollectionViewController : UICollectionViewController
@property NSArray *selectedServices;
- (IBAction)selectedJobCategories:(id)sender;
@end
