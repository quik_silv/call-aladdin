//
//  RequestTableViewController.h
//  facclient
//
//  Created by mark on 12/30/16.
//  Copyright © 2016 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface RequestTableViewController : UITableViewController <UIImagePickerControllerDelegate, AVAudioPlayerDelegate>

@property (strong, nonatomic) NSString* selectedRequestUuid;
@property float longitude;
@property float latitude;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UILabel *workLabel;
@property (weak, nonatomic) IBOutlet UITextField *workTextField;
@property (weak, nonatomic) IBOutlet UITextField *preferredStartDate;
@property (weak, nonatomic) IBOutlet UITextField *preferredEndDate;
@property (weak, nonatomic) IBOutlet UILabel *workCategoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *workCategoryImageView;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UILabel *selectedCityLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedCountryLabel;
@property (weak, nonatomic) NSString* workCategory;
@property (weak, nonatomic) NSString* workCategoryIconFilename;
@property (weak, nonatomic) NSString* cityName;
@property (weak, nonatomic) NSString* countryName;
- (IBAction)takePhoto:(id)sender;
- (void)setSelectedService:(NSString*)name;
- (void)setSelectedServiceIcon:(NSString*)filename;
- (void)setSelectedCity:(NSString*)name;
- (void)setSelectedCountry:(NSString*)name;
- (IBAction)submitButton:(id)sender;


- (IBAction)startRecord:(id)sender;
- (IBAction)stopRecord:(id)sender;
- (IBAction)startPlay:(id)sender;
- (IBAction)stopPlay:(id)sender;

//- (IBAction)overwriteRecording:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *overwriteRecordingLabel;
@property (weak, nonatomic) IBOutlet UILabel *recordingLabel;
//@property (weak, nonatomic) IBOutlet UIButton *speakerButton;
@property (weak, nonatomic) IBOutlet UIButton *microphoneButton;
@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UILabel *remainingTimeLabel;

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag;
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error;
- (void)updateDisplay;
- (void)updateSliderLabels;
- (IBAction)currentTimeSliderValueChanged:(id)sender;
- (IBAction)currentTimeSliderTouchUpInside:(id)sender;

- (void) createRequest;
- (void) updateRequest;

@end
