//
//  CountriesTableViewController.h
//  facclient
//
//  Created by mark on 8/17/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountriesTableViewController : UITableViewController
@property NSString * selectedCountry;
@end
