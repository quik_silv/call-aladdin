//
//  JobCategoriesCollectionViewController.m
//  
//
//  Created by mark on 2017/09/27.
//

#import "JobCategoriesCollectionViewController.h"
#import "UserRegistrationTableViewController.h"
#import "UserProfileTableViewController.h"

@interface JobCategoriesCollectionViewController ()
{
    NSArray *services;
    NSMutableArray *temporaryStoreForServicesSelection;
}
@end

@implementation JobCategoriesCollectionViewController
@synthesize selectedServices;
//static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    self.collectionView.allowsMultipleSelection = NO;
    temporaryStoreForServicesSelection = [NSMutableArray arrayWithArray:selectedServices];
    services = @[
        @{@"label":@"construction, tiling & painting", @"image": @"concrete-01"},
        @{@"label":@"wiring & lighting", @"image": @"bulb-01"},
        @{@"label":@"interior design & carpenters", @"image": @"cabinet-01"},
        @{@"label":@"laminated, timber flooring", @"image": @"wood-01"},
        @{@"label":@"curtain, carpet & wallpaper", @"image": @"curtain-01"},
        @{@"label":@"signboard", @"image": @"signboard-01"},
        @{@"label":@"air conditioner", @"image": @"aircond-01"},
        @{@"label":@"plastered ceiling", @"image": @"ceiling-01"},
        @{@"label":@"aluminium & glass works", @"image": @"window-01"},
        @{@"label":@"alarm & cctv", @"image": @"cctv-01"},
        @{@"label":@"cleaning services", @"image": @"vacuum-01"},
        @{@"label":@"landscaping & pond", @"image": @"tree-01"},
        @{@"label":@"gates & steel works", @"image": @"gate-01"},
        @{@"label":@"plumber", @"image": @"water tap-01"},
        @{@"label":@"pest control", @"image": @"termite-01"},
        @{@"label":@"fengshui consultation", @"image": @"fengshui-01"},
];
    
    // Register cell classes
    //[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [services count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"userRegisterJobSelectCell" forIndexPath:indexPath];
    
    // Configure the cell
    UIImageView *servicesImageView = (UIImageView *)[cell viewWithTag:100];
    [servicesImageView setImage: [UIImage imageNamed:[[services objectAtIndex:indexPath.row] objectForKey:@"image"] ] ];
    UILabel *servicesLabel = (UILabel *)[cell viewWithTag:99];
    [servicesLabel setText: [NSLocalizedString([[services objectAtIndex:indexPath.row] objectForKey:@"label"], nil) uppercaseString] ];
    UIView *selectedBackground = [[UIView alloc] initWithFrame:CGRectMake(0., 0., 100., 100.)];
    [selectedBackground setBackgroundColor:[UIColor grayColor] ];
    [selectedBackground setAlpha:0.4];
    [cell setSelectedBackgroundView:selectedBackground];
    
    NSUInteger index = [selectedServices indexOfObject:[[services objectAtIndex:indexPath.row] objectForKey:@"label"] ];
    if(index < [services count]) {
        [cell setSelected:YES];
        //UICollectionView does not know the cell was preselected therefore this.
        [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    }
    [cell setAccessibilityIdentifier:[NSString stringWithFormat:@"JobCategories-%@", [[services objectAtIndex:indexPath.row] objectForKey:@"label"]] ]; //for UITests
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Determine the selected items by using the indexPath
    NSString *object = [[services objectAtIndex:indexPath.row] objectForKey:@"label"];
    // Add the selected item into the array
    [temporaryStoreForServicesSelection addObject:object];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *object = [[services objectAtIndex:indexPath.row] objectForKey:@"label"];
    
    [temporaryStoreForServicesSelection removeObject:object];
}
/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/
- (IBAction)selectedJobCategories:(id)sender {
    NSArray *stack = [self.navigationController viewControllers];
    UIViewController *sourceViewController = [stack objectAtIndex:[stack count] - 2];
    if ([sourceViewController isKindOfClass:UserRegistrationTableViewController.class]) {
        [self performSegueWithIdentifier:@"selectJobCategoriesSegueForUserRegistration" sender:nil];
    }
    if ([sourceViewController isKindOfClass:UserProfileTableViewController.class]) {
        [self performSegueWithIdentifier:@"selectJobCategoriesSegueForUserProfile" sender:nil];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSUInteger index = [temporaryStoreForServicesSelection indexOfObject:@""];
    if (index < [temporaryStoreForServicesSelection count]) {
        [temporaryStoreForServicesSelection removeObjectAtIndex:index ];
    }
    if ([[segue identifier] isEqualToString:@"selectJobCategoriesSegueForUserRegistration"])
    {
        UserRegistrationTableViewController *destViewController = [segue destinationViewController];
        [destViewController setSelectedServices:[temporaryStoreForServicesSelection copy] ];
    }
    if ([[segue identifier] isEqualToString:@"selectJobCategoriesSegueForUserProfile"])
    {
        UserProfileTableViewController *destViewController = [segue destinationViewController];
        [destViewController setSelectedServices:[temporaryStoreForServicesSelection copy] ];
    }
}

@end
