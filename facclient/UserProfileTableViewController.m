//
//  UserProfileTableViewController.m
//  facclient
//
//  Created by mark on 8/29/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "RemoteJson.h"
#import "Spinner.h"
#import "UserProfileTableViewController.h"
#import "CitiesTableViewController.h"
#import "CountriesTableViewController.h"
#import "JobCategoriesCollectionViewController.h"
#import <SDWebImage/UIButton+WebCache.h>

@import Firebase;
@import FirebaseAuth;
@interface UserProfileTableViewController ()
{
    NSArray *userProfileData;
    int reloadTimes;
}
@end

@implementation UserProfileTableViewController
@synthesize nameLabel;
@synthesize nameTextField;
@synthesize phoneLabel;
@synthesize phoneTextField;
@synthesize emailLabel;
@synthesize emailTextField;
@synthesize addressLabel;
@synthesize addressTextField;
@synthesize contractorSwitch;
@synthesize photoButton;
@synthesize cityName;
@synthesize countryName;
@synthesize selectedCityLabel;
@synthesize selectedCountryLabel;
@synthesize workCategoryLabel;
@synthesize selectedWorkCategoryLabel;
@synthesize selectedServices;
@synthesize companyRegisteredAddressLabel;
@synthesize companyRegisteredAddressTextField;
@synthesize companyNameLabel;
@synthesize companyNameTextField;
@synthesize passwordRegisteredLabel;
- (void) reloadData {
    reloadTimes++;
    Spinner *spin = [[Spinner alloc] initWithView:self];
    [spin start];
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        //Personal dashboard. Can only view own pending requests
        NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles?uuid=%@", [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"uuid"] ];
        userProfileData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
        if ([userProfileData count] == 0) {
            //probably coming directly from UserRegistrationViewController and the data has not been saved into the database due to network conditions, proper way to do it would be from RemoteJson and continue only when the data is correctly sent. Here is a workaround because I have not much time.
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userProfileData"];
                NSError *signOutError;
                [[FIRAuth auth] signOut:&signOutError];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil)
                                                                    message:NSLocalizedString(@"Please register with us.", nil)
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil];
                [alertView show];
        } else {
            [self.navigationItem setTitle:[[userProfileData objectAtIndex:0] objectForKey:@"name"] ];
            [nameTextField setText:[[userProfileData objectAtIndex:0] objectForKey:@"name"] ];
            [phoneTextField setText:[[userProfileData objectAtIndex:0] objectForKey:@"phone"] ];
            [addressTextField setText:[[userProfileData objectAtIndex:0] objectForKey:@"address"] ];
            [emailTextField setText:[[userProfileData objectAtIndex:0] objectForKey:@"email"] ];
            [addressTextField sizeToFit];
            [selectedCityLabel setText:[[userProfileData objectAtIndex:0] objectForKey:@"city"] ];
            [selectedCountryLabel setText:[[userProfileData objectAtIndex:0] objectForKey:@"country"] ];
            
            if ([[[userProfileData objectAtIndex:0] objectForKey:@"is_contractor"] intValue]) {
                [contractorSwitch setOn:YES];
                [companyRegisteredAddressTextField setText:[[userProfileData objectAtIndex:0] objectForKey:@"company_address"] ];
                [companyNameTextField setText:[[userProfileData objectAtIndex:0] objectForKey:@"company_name"] ];
                
                //average rating calculation
                NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/requests?contractor=%@", [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"uuid"] ];
                NSArray *contractorRequestsData = [[r updateDataFromRemoteSourceFrom:dataSourceUrl] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF['rating']!=nil"] ];

                float averageRating = .0f;
                if ([contractorRequestsData count]) {
                    for (int q=0; q < [contractorRequestsData count]; q++) {
                        if(![[[contractorRequestsData objectAtIndex:q] objectForKey:@"rating"]  isEqual:[NSNull null] ])
                            averageRating += [[[contractorRequestsData objectAtIndex:q] objectForKey:@"rating"] floatValue] / [contractorRequestsData count];
                    }
                    [self.navigationItem setTitle:[NSString stringWithFormat:@"%@ (%.1f)", [[userProfileData objectAtIndex:0] objectForKey:@"company_name"], averageRating] ];
                } else {
                    [self.navigationItem setTitle:[NSString stringWithFormat:@"%@ (not rated)", [[userProfileData objectAtIndex:0] objectForKey:@"company_name"] ] ];
                }
                
                NSString *workCategoriesFlat = [[userProfileData objectAtIndex:0] objectForKey:@"work_categories"];
                [selectedWorkCategoryLabel setText:[NSLocalizedString(workCategoriesFlat, nil) uppercaseString] ];
                [self setSelectedServices:[workCategoriesFlat componentsSeparatedByString:@";"] ];
                //update user data on iphone, should use CoreData in the future
                [[NSUserDefaults standardUserDefaults] setObject:[userProfileData objectAtIndex:0] forKey:@"userProfileData"];
                
                [contractorSwitch setEnabled:NO];
                [phoneTextField setEnabled:NO];
//                [companyRegisteredAddressTextField setEnabled:NO];
//                [companyNameTextField setEnabled:NO];
            } else {
                [contractorSwitch setOn:NO];
            }
            
            NSURL *imageURL = [NSURL URLWithString:[[userProfileData objectAtIndex:0] objectForKey:@"image"] ];
            [photoButton sd_setImageWithURL:imageURL forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"upload-from-images-placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
            }];
            
            [[self tableView] reloadData];
        }
        [spin stop];
    });
}

- (void) getLatestData:(UIRefreshControl *)refreshControl {
    [self reloadData];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *updated = [NSString stringWithFormat:NSLocalizedString(@"Last update: %@", nil), [formatter stringFromDate:[NSDate date]]];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:updated];
    [self.refreshControl endRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self reloadData];
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor orangeColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestData:)
                  forControlEvents:UIControlEventValueChanged];
    
    //set all UITextField delegate to self
    phoneTextField.delegate = (id)self;
    emailTextField.delegate = (id)self;
    addressTextField.delegate = (id)self;
    
//    FIRUser *user = [FIRAuth auth].currentUser;
//    NSLog(@"%@ %@ %@", user.uid, user.email, user.phoneNumber);
//    [passwordRegisteredLabel setHidden:YES];
//    if (user.email) {
//        [passwordRegisteredLabel setHidden:NO];
//    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //register App Messaging topics
    NSString *escapedWorkCategories = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"work_categories"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    escapedWorkCategories = [escapedWorkCategories stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    escapedWorkCategories = [escapedWorkCategories stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    [[FIRMessaging messaging] subscribeToTopic:escapedWorkCategories
                                    completion:^(NSError * _Nullable error) {
//                                        NSLog(@"Subscribed to %@ topic", [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"work_categories"]);
                                    }];
    NSString *escapedCity = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"city"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    [[FIRMessaging messaging] subscribeToTopic:escapedCity
                                    completion:^(NSError * _Nullable error) {
//                                        NSLog(@"Subscribed to %@ topic", [[[NSUserDefaults standardUserDefaults] objectForKey:@"userProfileData"] valueForKey:@"city"]);
                                    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 11;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0.0;
    switch(indexPath.row) {
        case 0:
            height = 250.;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 10:    //submit button
            height = 44.;
            break;
        case 7:
        case 8:
        case 9:
            height = 0.; //64.
            if ([contractorSwitch isOn]) {
                height = 64.;
            }
            break;
    }

    return height;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"selectJobCategorySegueForUserProfile"]) {
        if([[[userProfileData objectAtIndex:0] objectForKey:@"is_contractor"] intValue] ) {
            //is_contractor, do not allow change
            return FALSE;
        }
    }
    return TRUE;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[[userProfileData objectAtIndex:0] objectForKey:@"is_contractor"] intValue]) {
        //is contractor don't allow change of job category
    } else {
        if ([[segue identifier] isEqualToString:@"selectJobCategorySegueForUserProfile"])
        {
            JobCategoriesCollectionViewController *destViewController = [segue destinationViewController];
            [destViewController setSelectedServices:selectedServices];
        }
    }
}

- (IBAction)toggleContractorSwitch:(id)sender {
    [self.tableView reloadData];
}
- (IBAction)takePhoto:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    [actionSheet setDelegate:(id)self];
    [actionSheet setTitle:@"Photos"];
    [actionSheet addButtonWithTitle:@"Camera"];
    [actionSheet addButtonWithTitle:@"Library"];
    [actionSheet showInView:self.tableView];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if(buttonIndex == 0) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else if(buttonIndex == 1) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [photoButton setImage:chosenImage forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)editUser:(id)sender {
    BOOL empty = NO;
    if (![contractorSwitch isOn]) {
        [companyNameTextField setText:@""];
        [companyRegisteredAddressTextField setText:@""];
    } else {
        if ([[companyNameTextField text] length] == 0) {
            companyNameTextField.layer.borderColor = [[UIColor redColor] CGColor];
            companyNameTextField.layer.borderWidth = 1;
            empty = YES;
        }
        if ([[companyRegisteredAddressTextField text] length] == 0) {
            companyRegisteredAddressTextField.layer.borderColor = [[UIColor redColor] CGColor];
            companyRegisteredAddressTextField.layer.borderWidth = 1;
            empty = YES;
        }
    }
    UIColor *defaultColor = [UIColor blackColor];
    CGFloat defaultTextFieldBorderWidth = 0.0f;
    addressLabel.textColor = defaultColor;
    addressTextField.layer.borderWidth = defaultTextFieldBorderWidth;
    if ([[emailTextField text] length] == 0) {
        emailLabel.textColor = [UIColor redColor];
        emailTextField.layer.borderColor = [[UIColor redColor] CGColor];
        emailTextField.layer.borderWidth = 1;
        empty = YES;
    }
    if ([[addressTextField text] length] == 0) {
        addressLabel.textColor = [UIColor redColor];
        addressTextField.layer.borderColor = [[UIColor redColor] CGColor];
        addressTextField.layer.borderWidth = 1;
        empty = YES;
    }
    NSString *workCategoriesFlat = [selectedServices componentsJoinedByString:@";"];
    if ([contractorSwitch isOn]) {
        if ([[companyNameTextField text] length] == 0) {
            companyNameLabel.textColor = [UIColor redColor];
            companyNameTextField.layer.borderColor = [[UIColor redColor] CGColor];
            companyNameTextField.layer.borderWidth = 1;
            empty = YES;
        }
        if ([[companyRegisteredAddressTextField text] length] == 0) {
            companyRegisteredAddressLabel.textColor = [UIColor redColor];
            companyRegisteredAddressTextField.layer.borderColor = [[UIColor redColor] CGColor];
            companyRegisteredAddressTextField.layer.borderWidth = 1;
            empty = YES;
        }
        if ([workCategoriesFlat isEqualToString:@""]) {
            workCategoryLabel.textColor = [UIColor redColor];
            empty = YES;
        }
    } else {
        [companyNameTextField setText:@""];
        [companyRegisteredAddressTextField setText:@""];
        workCategoriesFlat = @"";
    }
    if(!empty) {
        NSDictionary *identity = @{
                                   @"uuid": [[userProfileData objectAtIndex:0] objectForKey:@"uuid"],
                                   @"email":[emailTextField text],
                                   @"address":[addressTextField text],
                                   @"phone":[phoneTextField text],
                                   @"city":[selectedCityLabel text],
                                   @"country":[selectedCountryLabel text],
                                   @"is_contractor":[contractorSwitch isOn] ? @"1" : @"0",
                                   @"company_name": [companyNameTextField text],
                                   @"company_address": [companyRegisteredAddressTextField text],
                                   @"work_categories":workCategoriesFlat,
                                   };
        Spinner *spin = [[Spinner alloc] initWithView:self];
        [spin start];
        RemoteJson *r = [[RemoteJson alloc] init];
        dispatch_async(dispatch_get_main_queue(), ^{
            [r editIdentity:identity withImage:UIImagePNGRepresentation([[photoButton imageView] image]) ];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Information", nil)
                                                                message:NSLocalizedString(@"Profile edited", nil)
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            [spin stop];
        });
    }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        UIViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"]; //previously mainNavigation
        [self presentViewController:controller animated:YES completion:Nil];
    }
}
- (void)setSelectedCity:(NSString *)name {
    cityName = name;
}
- (void)setSelectedCountry:(NSString *)name {
    countryName = name;
}
//- (void)setSelectedServices:(NSArray *)array {
//    selectedServices = [NSArray arrayWithArray:array];
//}
-(IBAction)unwindFromCitySelectionToUserProfile:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[CitiesTableViewController class]]) {
        [selectedCityLabel setText:cityName];
    }
}
-(IBAction)unwindFromCountrySelectionToUserProfile:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[CountriesTableViewController class]]) {
        [selectedCountryLabel setText:countryName];
    }
}
-(IBAction)unwindFromJobCategorySelectionToUserProfile:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[JobCategoriesCollectionViewController class]]) {
        [selectedWorkCategoryLabel setText:[NSLocalizedString([selectedServices objectAtIndex:0], nil) uppercaseString] ];
    }
}
@end
