//
//  TnCTableViewController.m
//  facclient
//
//  Created by mark on 2017/09/27.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "TnCTableViewController.h"

@interface TnCTableViewController ()
{
    NSArray *tncData;
}
@end

@implementation TnCTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    tncData = @[
                @{@"title": @"terms and conditions", @"content": @"Call Aladdin is a marketplace platform for services performed by its users. The following terms and conditions govern your access to and use of the Call Aladdin website or mobile app, including any content, functionality and services offered on or through [call-aladdin.com (\"the website\") or CallAladdin (\"mobile app\")]. Please read the terms and conditions carefully before you start to use the website or mobile app. By using the website or mobile app, you agree to follow and be bound by these terms and conditions in full. If you disagree with these terms and conditions or any part of these terms and conditions, you should not use this website or mobile app."},
                @{@"title": @"definitions", @"content": @"\"Contractor(s)\" are users who offer services through our website or mobile app.\n\"Client(s)\" are users who procure the services offered by the Contractors through our website or mobile app."},
                @{@"title": @"overview", @"content":@"Only registered users may offer or procure services on our website or mobile app."},
                @{@"title": @"disclaimer", @"content":@"We reserve the right to modify the terms and conditions and to add new or additional terms or conditions on your use of our website or mobile app at any time and at our sole discretion. Such modifications and additional terms and conditions will be effective immediately without notice to you. Your continued use of this website or mobile app will be deemed acceptance thereof. We also reserve the right to terminate your usage of our website or mobile app at any time and at our sole discretion without notice to you. The information contained in this website or mobile app is for general information purposes only and is not legal advice or other professional advice. While we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or mobile app or the information, products, services, or related graphics contained on the website or mobile app for any purpose. Any reliance you place on such information is therefore strictly at your own risk. Nothing on this website or mobile app should be taken to constitute professional advice or formal recommendations and we exclude all representations and warranties relating to the content and use of this website or mobile app. While the content of this website or mobile app is provided in good faith, we do not warrant that the information will be kept up to date, be true and not misleading, or that this website or mobile app will always (or ever) be available for use. Every effort is made to keep the website or mobile app up and running smoothly. However, we take no responsibility for, and will not be liable for, the website or mobile app being temporarily unavailable due to technical issues beyond our control.\nWe do not warrant that the servers that make this website or mobile app available will be error, virus or bug free and you accept that it is your responsibility to make adequate provision for protection against such threats. Our website or mobile app makes no express or implied warranty for merchantability, fitness for a particular purpose or otherwise or all other warranties expressed or implied including the warranty of merchantability and the warranty of fitness for a particular purpose are expressly excluded or disclaimed. "},
                @{@"title": @"dispute", @"content":@"Our company is a technology company that does not provide or engage in the services provided by the Contractors and our company is not a Contractor providing services to Clients. The website or mobile app are intended to be used for facilitating the Contractors to offer their services to the Clients. Our company is not responsible or liable for the acts and/or omissions of any services provided by the Contractors to the Clients, for the acts and/or omissions of payment by the Clients to the Contractors and for any criminal and/or illegal action committed by the Contractors or the Clients. The Contractors shall, at all time, not claim or cause any person to misunderstand that the Contractors are the agent, employee or staff of our company, and the services provided by the Contractors are not, in any way, be deemed as services of our company. Furthermore, we are not responsible for the content, quality or the level of service provided by the Contractors. We provide no warranty with respect to the services provided by the Contractors, their delivery, and any communications between Clients and the Contractors. We encourage Clients to take advantage of our rating system, our community and common sense in choosing appropriate service offers. We encourage Clients and Contractors to try and settle conflicts amongst themselves."},
                @{@"title": @"limitation of liability", @"content":@"In no event will we be liable for any incidental, indirect, consequential or special damage of any kind, or any damages whatsoever, including, without limitation, those resulting from loss of profit, loss of contracts, goodwill, data, information, income, anticipated savings or business relationships, whether or not advised of the possibility of such damage, arising out of or in connection with the use of this website or mobile app or any linked websites or mobile apps."},
                @{@"title": @"links", @"content":@"This website or mobile app may include links providing direct access to other websites or internet resources. We have no control over the nature, content and availability of those websites or internet resources. These links are included for convenience, and the inclusion of any link does not necessarily imply a recommendation or endorse the views expressed within them. We are also not responsible for the accuracy or content of information contained in these websites or internet resources."},
                @{@"title": @"users", @"content":@"You may view, download for caching purposes only, and print pages (or other content) from the website or mobile app for your own personal use, but you must not:-\ni) Republish material from this website or mobile app (including republication on another website);\nii) Sell, rent or sub-licence material from the website or mobile app;\niii) Reproduce, duplicate, copy or otherwise exploit material on our website or mobile app for a commercial purpose;\niv) Edit or otherwise modify any material on the website or mobile app; or\nv) Redistribute material from this website or mobile app.\nYou must not use our website or mobile app in any way that causes, or may cause damage to the website or impairment of the availability or accessibility of the website, or in any way which is unlawful, illegal, fraudulent or harmful or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity. In case of any violation of the terms and conditions, we reserve the right to seek all remedies available to it in law and in equity. We reserve the right, at our sole discretion and at any time, to terminate your access to the website, mobile app and/or any of its services without notice to you and without liability to you or any third party."},
                @{@"title": @"indemnity", @"content":@"By using this website or mobile app, you agree that you shall defend, indemnify and hold our company, its licensors and each such party’s parent organizations, subsidiaries, affiliates, officers, directors, members, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys’ fees and costs) arising out of or in connection with: (a) your violation or breach of any term of this Agreement or any applicable law or regulation, including any local laws or ordinances, whether or not referenced herein; (b) your violation of any rights of any third party, including, but not limited to the Contractors or Clients, as a result of your own interaction with any third party; and (c) your use (or misuse) of the website or mobile app. You agree that you shall not sue or recover any damages from us as a result of our decision to remove or refuse to process any information or content, to warn you, to suspend or terminate your access to our website or mobile app."},
                @{@"title": @"entire agreement", @"content": @"These terms and conditions constitute the entire agreement between you and us in relation to your use of our website or mobile app and any disputes relating to these terms and conditions will be subject to the relevant governing law."},
                ];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UIView *tableFooter = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [tncData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[[tncData objectAtIndex:section] objectForKey:@"title"] uppercaseString];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
//    CGSize preferedSize = CGSizeMake(tableView.frame.size.width, 1000);
//    CGSize size = [[[tncData objectAtIndex:indexPath.section] objectForKey:@"content"]
//                        boundingRectWithSize:preferedSize
//                        options:NSStringDrawingUsesLineFragmentOrigin
//                        attributes:@{
//                                     NSFontAttributeName : font
//                                     }
//                        context:nil].size;
//
//    return size.height;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tncCell" forIndexPath:indexPath];
    // Configure the cell...
    [cell.textLabel setNumberOfLines:100]; //random choice of a big number
    [cell.textLabel setText:[[tncData objectAtIndex:indexPath.section] objectForKey:@"content"] ];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
