//
//  CurrentHistoryTableViewController.h
//  facclient
//
//  Created by mark on 9/17/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"

@interface CurrentHistoryTableViewController : UITableViewController <RateViewDelegate, UITextViewDelegate>
@property (strong, nonatomic) NSString *selectedRequestUuid;
@property (weak, nonatomic) IBOutlet UILabel *contractorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
- (IBAction)rateRequest:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *rateButton;

@property (weak, nonatomic) IBOutlet RateView *rateView;
@property (assign, nonatomic) float userRating;
@end
