//
//  HistoryTableViewController.h
//  facclient
//
//  Created by mark on 8/5/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewController : UITableViewController
@property (weak, nonatomic) NSString* selectedRequestUuid;
@end
