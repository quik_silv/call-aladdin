//
//  CitiesTableViewController.h
//  facclient
//
//  Created by mark on 8/16/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitiesTableViewController : UITableViewController
@property NSString * selectedCity;
@end
