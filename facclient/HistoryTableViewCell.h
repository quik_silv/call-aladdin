//
//  HistoryTableViewCell.h
//  facclient
//
//  Created by mark on 8/5/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"

@interface HistoryTableViewCell : UITableViewCell <RateViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *workCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *contractorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *contractorImageView;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;

@end
