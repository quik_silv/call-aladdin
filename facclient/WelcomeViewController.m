//
//  WelcomeViewController.m
//  facclient
//
//  Created by mark on 3/14/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "WelcomeViewController.h"
#import "RemoteJson.h"
#import "Spinner.h"

@import Firebase;
@import FirebaseAuth;
@import FirebaseUI;
@import FirebaseInstanceID;
@interface WelcomeViewController () <FUIAuthDelegate>
{
    NSArray *queryData;
}
@end

@implementation WelcomeViewController
@synthesize registerButton;

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)userLogin:(id)sender {
    [FUIAuth defaultAuthUI].delegate = (id) self; // delegate should be retained by you!
    FUIPhoneAuth *phoneAuthProvider = [[FUIPhoneAuth alloc] initWithAuthUI:[FUIAuth defaultAuthUI]];
    [FUIAuth defaultAuthUI].providers = @[phoneAuthProvider];
    
    FUIPhoneAuth *phoneProvider = [FUIAuth defaultAuthUI].providers.firstObject;
    [phoneProvider signInWithPresentingViewController:self phoneNumber:nil ];
}

#pragma mark - FUIAuthDelegate methods

- (void)authUI:(FUIAuth *)authUI didSignInWithAuthDataResult:(nullable FIRAuthDataResult *)authDataResult
         error:(nullable NSError *)error {
    if (error) {
        if (error.code == FUIAuthErrorCodeUserCancelledSignIn) {
            [self showAlert:NSLocalizedString(@"User cancelled sign-in", nil)];
        } else {
            NSError *detailedError = error.userInfo[NSUnderlyingErrorKey];
            if (!detailedError) {
                detailedError = error;
            }
            [self showAlert:detailedError.localizedDescription];
        }
    } else {
        if ([FIRAuth auth].currentUser) {
            NSString *dataSourceUrl = [NSString stringWithFormat:@"https://facontractor.herokuapp.com/api/user_profiles?phone=%@", [FIRAuth auth].currentUser.phoneNumber];
            RemoteJson *r = [[RemoteJson alloc] init];
            NSArray *queryData = [r updateDataFromRemoteSourceFrom:dataSourceUrl];
            NSLog(@"%@", queryData);
            if([queryData count]) {
                [[NSUserDefaults standardUserDefaults] setObject:[queryData objectAtIndex:0] forKey:@"userProfileData"];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UITabBarController *tabController = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigation"];
                [self presentViewController:tabController animated:YES completion:Nil];
            }
        } else {
            NSLog(@"Error");
        }
    }
}

- (void)showAlert:(NSString *)message {
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil)
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* closeButton =
    [UIAlertAction actionWithTitle:NSLocalizedString(@"Close", nil)
                             style:UIAlertActionStyleDefault
                           handler:nil];
    [alert addAction:closeButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
@end
