//
//  CitiesTableViewController.m
//  facclient
//
//  Created by mark on 8/16/17.
//  Copyright © 2017 Square Potato. All rights reserved.
//

#import "CitiesTableViewController.h"
#import "RequestTableViewController.h"
#import "UserRegistrationTableViewController.h"
#import "UserProfileTableViewController.h"
#import "RemoteJson.h"
#import "Spinner.h"

@interface CitiesTableViewController ()
{
    NSArray *queryData;
}
@end

@implementation CitiesTableViewController
@synthesize selectedCity;

- (void)reloadData {
    Spinner *spin = [[Spinner alloc] initWithView:self];
    [spin start];
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        queryData = [r updateDataFromRemoteSourceFrom:@"https://facontractor.herokuapp.com/api/cities/"];
        if (![queryData count]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information"
                                                                message:@"There is no data."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
        } else {
            [[self tableView] reloadData];
        }
        [spin stop];
    });
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [queryData count];;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"cityCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell.textLabel setText:[[queryData objectAtIndex:indexPath.row] objectForKey:@"name"] ];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedCity = [[queryData objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSArray *stack = [self.navigationController viewControllers];
    UIViewController *sourceViewController = [stack objectAtIndex:[stack count] - 2];
    if ([sourceViewController isKindOfClass:RequestTableViewController.class]) {
        [self performSegueWithIdentifier:@"selectCitySegueForRequestCreation" sender:nil];
    }
    if ([sourceViewController isKindOfClass:UserRegistrationTableViewController.class]) {
        [self performSegueWithIdentifier:@"selectCitySegueForUserRegistration" sender:nil];
    }
    if ([sourceViewController isKindOfClass:UserProfileTableViewController.class]) {
        [self performSegueWithIdentifier:@"selectCitySegueForUserProfile" sender:nil];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"selectCitySegueForRequestCreation"])
    {
        RequestTableViewController *destViewController = [segue destinationViewController];
        [destViewController setSelectedCity:selectedCity];
    }
    if ([[segue identifier] isEqualToString:@"selectCitySegueForUserRegistration"])
    {
        UserRegistrationTableViewController *destViewController = [segue destinationViewController];
        [destViewController setSelectedCity:selectedCity];
    }
    if ([[segue identifier] isEqualToString:@"selectCitySegueForUserProfile"])
    {
        UserProfileTableViewController *destViewController = [segue destinationViewController];
        [destViewController setSelectedCity:selectedCity];
    }
}

@end
